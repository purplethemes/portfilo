<?php
/*
Plugin Name: Testimonial

Plugin URI: http://www.augustinfotech.com

Description: Testimonial is a simple WordPress plugin to showcase your testimonials on your website using shortcode.

Version: 1.0

Text Domain: wpt

Author: August Infotech

Author URI: http://www.augustinfotech.com
*/

define( 'TESTI_PLUGIN_URL', 			plugin_dir_url( __FILE__) );
define( 'TESTI_PLUGIN_PATH',			plugin_dir_path( __FILE__ ) );
define( 'TESTI_PLUGIN_BASENAME', 		plugin_basename( __FILE__ ) );

/* ---------------------------------------------------------------------------
 * Load the plugin required files
 * --------------------------------------------------------------------------- */

function testimonial_plugin_load_function(){
	
	require_once( 'wpt-posttype-testimonial.php' );
	
	require_once('testimonial-shortcode.php');
	
}
add_action( 'plugins_loaded','testimonial_plugin_load_function' );
        	
/* ---------------------------------------------------------------------------
 * Activate Hook Plugin
 * --------------------------------------------------------------------------- */
 
if ( function_exists('register_activation_hook') )
register_activation_hook(__FILE__,'testimonial_plugin_activate');

if ( ! function_exists( 'testimonial_plugin_activate' ) ) :
function testimonial_plugin_activate() {
	
	add_option('testimonial_title','Gallery','', 'yes');
	add_option('testimonial_content','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','', 'yes');
    
}
endif; //testimonial_plugin_activate

/* ---------------------------------------------------------------------------
 * Uninstall Hook Plugin
 * --------------------------------------------------------------------------- */

if ( function_exists('register_uninstall_hook') )
register_uninstall_hook(__FILE__,'wpt_testimonial_plugin_droped'); 

if ( ! function_exists( 'wpt_testimonial_plugin_droped' ) ) :

function wpt_testimonial_plugin_droped() { 

	delete_option('testimonial_title','Gallery','', 'yes');
	delete_option('testimonial_content','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','', 'yes');

}
endif; //wpt_testimonial_plugin_droped

?>