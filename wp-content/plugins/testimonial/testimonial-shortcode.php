<?php

add_shortcode('testimonial', 'testimonialShortcode');
function testimonialShortcode($atts, $content = null) 
{
	/*wp_enqueue_style('testimonial-style',  plugins_url('/testimonial/css/style.css'));
	wp_enqueue_style('testimonial-animate',  plugins_url('/testimonial/css/animate.css'));
	
	wp_enqueue_style('testimonial-bootstrap',  plugins_url('/testimonial/css/bootstrap.css'));
	
	wp_enqueue_style('testimonial-component',  plugins_url('/testimonial/css/component.css'));
	
	wp_enqueue_style('testimonial-content',  plugins_url('/testimonial/css/content.css'));
	
	wp_enqueue_style('testimonial-demo',  plugins_url('/testimonial/css/demo.css'));
	
	wp_enqueue_style('testimonial-font-awesome.min',  plugins_url('/testimonial/css/font-awesome.min.css'));
	
	wp_enqueue_style('testimonial-jquery.bxslider',  plugins_url('/testimonial/css/jquery.bxslider.css'));
	
	wp_enqueue_style('testimonial-lightbox',  plugins_url('/testimonial/css/lightbox.css'));
	
	wp_enqueue_style('testimonial-owl.carousel',  plugins_url('/testimonial/css/owl.carousel.css'));
	
	wp_enqueue_style('testimonial-owl.theme.default.min',  plugins_url('/testimonial/css/owl.theme.default.min.css'));
	
	wp_enqueue_script( 'testimonial-jquery-1.11.0.min', plugins_url('/testimonial/js/jquery.jquery-1.11.0.min.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-bootstrap.min', plugins_url('/testimonial/js/bootstrap.min.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-bootstrap-tabcollapse', plugins_url('/testimonial/js/bootstrap-tabcollapse.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-jquery.bxslider.min', plugins_url('/testimonial/js/jquery.bxslider.min.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-jquery.counterup.min', plugins_url('/testimonial/js/jquery.counterup.min.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-jquery.firefly', plugins_url('/testimonial/js/jquery.counterup.min.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-jquery.flexslider-min', plugins_url('/testimonial/js/jquery.flexslider-min.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-jquery.form', plugins_url('/testimonial/js/jquery.form.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-jquery.isotope', plugins_url('/testimonial/js/jquery.isotope.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-jquery.nicescroll.min', plugins_url('/testimonial/js/jquery.nicescroll.min.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-jquery.validate', plugins_url('/testimonial/js/jquery.jquery.validate.js'), array( 'jquery' ));
	
	
	wp_enqueue_script( 'testimonial-lightbox.min', plugins_url('/testimonial/js/jquery.lightbox.min.js'), array( 'jquery' ));
	
	
	
	wp_enqueue_script( 'testimonial-owl.carousel.min', plugins_url('/testimonial/js/owl.carousel.min.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-wow.min', plugins_url('/testimonial/js/wow.min.js'), array( 'jquery' ));
	
	wp_enqueue_script( 'testimonial-main', plugins_url('/testimonial/js/main.js'), array( 'jquery' ));*/
	
	wp_head(); 
	
	extract(shortcode_atts(array(
		'count' => '4',
		'order' => 'DESC',
		'orderby' => 'menu_order',
        'category' => '',
    ), $atts)); 
	
	
	$args=array(
		'post_type' => 'testimonial',
		'posts_per_page' => intval($count),
		'order' => $order,
		'orderby' => $orderby,

	);
	
	if( $category ) {
		$args['testimonial_category'] = $category;
	}
	
	$query = new WP_Query($args);

    if ($query->have_posts())
	{ 
	
	$testimonial_title = get_option('testimonial_title');
	$testimonial_content = get_option('testimonial_content');
	
		
	    	$html .='<section class="exp-section">';
		        $html .='<div class="title-area wow  fadeIn">';
		            $html .='<h2 class="section-title">' .$testimonial_title.'</h2>';
		            $html .='<div class="section-divider divider-inside-top"></div>';
		            $html .= $testimonial_content ? '<p class="section-sub-text">'.$testimonial_content.'</p>' : '';
		            
		        	$html .='</div>';
					
					$html .='<section class="testimonials-slider wow flipInX" data-wow-delay="0.3s">';
					
					while($query->have_posts()){
						$query->the_post();
					    $testimonial_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ));
	                    $testimonial_imgURL = $testimonial_imgArray[0];
						$html .='<article class="slide">';
							$html .='<div class="testimonial-image">';
								$html .='<div class="bubble-image">';
							
							    	if(!empty($testimonial_imgURL)) {  
							    	$html .='<img alt="black" class="attachment-small wp-post-image" src="' .$testimonial_imgURL.'" title="exp" />';
							    	 } 
							    	 else { 
							    	$html .='<img alt="black" class="attachment-small wp-post-image" src="' .TESTI_PLUGIN_URL. '/images/no-img-portfolio.jpg'. '" style="width:87px;height:88px;" title="exp" />';
							    	 } 
									$html .='<div class="substrate">';
					                    $html .='<img alt="" src="' .TESTI_PLUGIN_URL. '/images/testimonial_bg.png'. '">';
					            	$html .='</div>';
							    $html .='</div>';
							$html .='</div>';
							               		
							$html .='<div class="testimonials-carousel-context">';
							    $html .='<div class="testimonials-carousel-content">';
							       $html .=' <p>'.get_the_content(). '</p>';
							       $html .=' <div class="testimonials-name">-' .get_the_title().'</div>';
							    $html .='</div>';
							$html .='</div>';
							
						$html .='</article>';
		
						
				}
			
	}
    
	wp_reset_query();
	
	
	$html .='</section>';	
    $html .='</section>';
    
	return $html;
}


?>