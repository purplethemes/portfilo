<?php
/**
 * The template used for displaying home page contents.
 *
 * @package Portfolio
 * @author Purplethemes
 */
?>

<?php
global $wp_query;

if(get_theme_mod( 'hp_checkbox' ) != ''){ ?>
	<!-- our services section-->
		<section class="our_service">
		    <div class="title-area wow fadeIn">
		        <?php if (get_theme_mod( 'hp_title' )) : ?><h2 class="section-title"><?php echo get_theme_mod( 'hp_title' ); ?></h2>
		        <?php else : ?><h2 class="section-title"><?php _e('', 'wpt'); ?></h2><?php endif; ?>
		        <div class="section-divider divider-inside-top"></div>
		        <?php if ( get_theme_mod( 'hp_content' ) ) : ?><p class="section-sub-text"><?php echo get_theme_mod( 'hp_content' ); ?></p>
		        <?php else : ?><p class="section-sub-text"><?php _e('', 'wpt'); ?></p><?php endif; ?>
		    </div>
		                
		    <article class="service-list">
		        <section class="row">
		        <?php 
		            
		            $cnt = 0;

		            for($i=1; $i<=3 ;$i++){
		                if( get_theme_mod( 'hp_subsection_'.$i ) != '')
		                {
		                    $cnt=$cnt+1;
		                }
		            }
		            
		            if( $cnt == 1 ):
		              $col = " col-sm-12 col-md-12";
		            elseif( $cnt == 2 ):
		              $col = " col-sm-6 col-md-6";
		            else:
		              $col = " col-sm-4 col-md-4";
		            endif;  
		            
		            for($i=1; $i<=3; $i++)
		                {
		                    if(get_theme_mod( 'hp_subsection_'.$i ) != '')
		                    {
		                        echo '<div class="service-container wow fadeInUp" data-wow-delay="0.4s">';
		                        echo '<article class="col-xs-12'. $col .'">';
		                        echo get_theme_mod( 'hp_subsection_'.$i );
		                        echo '</article>';
		                        echo '</div>';
		                    }
		                }
		            
		            ?>
		                      
		        </section>
		    </article>
		</section>
		<!-- our services section end -->
<?php } 

$post_id = $wp_query->get_queried_object_id();
$args=array(
  'page_id' => $post_id,
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 1,
  'caller_get_posts'=> 1
);

$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post ); 
 the_content();
endforeach;
wp_reset_postdata();

?>


				  
			 
