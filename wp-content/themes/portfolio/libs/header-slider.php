<!-- image slider section -->

<div class="slider-banner">
    <ul class="slides">
     <?php
      	for($i=1; $i<4; $i++) {
					
					if( get_theme_mod( 'slider_image_'.$i) != '' ) { ?>
						<li style="background-image: url('<?php echo get_theme_mod( 'slider_image_'.$i); ?>');" class="slide">
                   <?php } else { ?>
				   		<li style="background-image: url('<?php echo THEME_URI. '/images/no-img-portfolio.jpg' ?>');" class="slide">
				   <?php } ?>
	                    <div class="slider-overlay"></div>
	                    <div class="info" style="margin-top: 0px; opacity: 1;">
	                    	<?php if( get_theme_mod( 'slider_title_'.$i) != '' ) { ?>
	                        <h2><?php echo get_theme_mod( 'slider_title_'.$i); ?></h2>
	                        <?php } ?>
	                        <?php if( get_theme_mod( 'slider_content_'.$i) != '' ) { ?>
	                        <p><?php echo get_theme_mod( 'slider_content_'.$i); ?></p>
	                        <?php } ?>
	                        <?php if( get_theme_mod( 'button_text_'.$i) != '' ) {?>
	                        <a class="btn-white" href="<?php echo get_theme_mod( 'button_link_'.$i); ?>"><?php echo get_theme_mod( 'button_text_'.$i); ?></a>
	                        <?php } ?>
	                    </div>
                    </li>
		  <?php } ?>
			     
            
    </ul>
        
    <ul class="flex-direction-nav">
        
        <li><a href="#" class="flex-prev">Previous</a></li>
           
        <li><a href="#" class="flex-next">Next</a></li>
            
    </ul>
</div>
    
<!-- image slider section end -->
