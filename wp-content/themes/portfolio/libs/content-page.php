<?php
/**
 * The template used for displaying page content in page.php and Page Templates
 *
 * @package Portfolio
 * @author Purplethemes
 */

global $wp_query;

$post_id = $wp_query->get_queried_object_id();
$args=array(
  'page_id' => $post_id,
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 1,
  'caller_get_posts'=> 1
);

$myposts = get_posts( $args );


$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'aboutus-thumbnail' ) );

if(!empty($url)){
	$img_class = 'col-xs-12 col-sm-12 col-md-12 wow fadeInLeft';
	$content_class = 'col-xs-12 col-sm-12 col-md-12 wow fadeInRight';
	
}
else{
	$img_class = '';
	$content_class = 'col-xs-12 col-sm-12 col-md-12 wow fadeInRight';
}


foreach ( $myposts as $post ) : setup_postdata( $post ); 

	 if(!empty($url)){ ?>
				<article class="<?php echo $img_class; ?>" data-wow-delay="0.5s">
					<img class="marbottom img-responsive" alt="<?php _e('about', 'wpt'); ?>" title="about" src="<?php echo $url; ?>">
				</article>
				<?php } ?>

				<article class="<?php echo $content_class; ?>" data-wow-delay="1s">                  
					<div class="overview">
				        <h3><?php the_title();  ?></h3>
				        <div class="title-divider"></div>
				        <?php the_content(); ?>
				    </div>
				</article>

<?php endforeach; 
wp_reset_postdata();?>                					