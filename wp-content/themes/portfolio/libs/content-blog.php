<?php
/**
 * The template for displaying blog content
 *
 * @package Portfolio
 * @author Purplethemes
 */
 
global $wp_query; 

$post_id = $wp_query->get_queried_object_id(); 
$blog_detail_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'blog-detail-thumbnail' ) );  
?>
 
		<figure class="news wow fadeInLeft" data-wow-delay="0.3s">
            <div <?php post_class( 'news-img' ); ?>>
            <?php if(!empty($blog_detail_url)) { ?>
                <img class="img-responsive" src="<?php echo $blog_detail_url; ?>" alt="<?php _e('News Title', 'wpt'); ?>" />
                <?php } else { ?>
					<img class="img-responsive" src="<?php echo THEME_URI. '/images/no-img-portfolio.jpg'; ?>"  alt="<?php _e('News Title', 'wpt'); ?>" />	
				<?php } ?> 
            </div>
            <figcaption class="news-info">
                <h2><?php the_title(); ?></h2>
                <div class="meta">
                   <span class="date">
                     <i class="glyphicon glyphicon-calendar"></i>
                     <a href="<?php the_permalink(); ?>"><?php echo get_the_date(); ?></a>
                   </span>
                   <span class="category">
                     <i class="glyphicon glyphicon-tag"></i> 
                	 <?php 
                	 $tags_list = get_the_tag_list( '', _x( ', ', 'Used between list items, there is a space after the comma.', 'wpt' ) ); 
                	 if ( $tags_list ) {
					  printf($tags_list);
				     }?>
				   </span> 
                </div>
                <p><?php the_content(); ?></p>
            </figcaption>
        </figure>
        <div class="news-detail-divider"></div>
        
         <!-- news sharing section -->
        <div class="btn-group sharing-btns">
            <button class="btn btn-default disabled"><?php _e( 'Share:', 'wpt' ); ?></button>    
            
                <a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(the_permalink('','',false)); ?>&amp;t=<?php echo strip_tags($post->post_title); ?>" target="_blank" class="btn btn-default facebook"> <i class="fa fa-facebook fa-lg fb"></i></a>
              
                <a href="https://twitter.com/intent/tweet?original_referer=<?php echo urlencode(the_permalink('','',false)); ?>&amp;text=<?php echo strip_tags($post->post_title); ?>&amp;tw_p=tweetbutton&amp;url=<?php echo urlencode(the_permalink('','',false)); ?>" target="_blank" class="btn btn-default twitter"><i class="fa fa-twitter fa-lg tw"></i></a>
              
                <a href="//pinterest.com/pin/create/button/?url=<?php echo urlencode(the_permalink('','',false)); ?>&amp;media=<?php echo $blog_detail_url; ?>&amp;description=<?php echo strip_tags($post->post_title); ?>" target="_blank" class="btn btn-default pinterest"> <i class="fa fa-pinterest fa-lg pinterest"></i></a>
              
                <a href="https://plusone.google.com/_/+1/confirm?hl=<?php language_attributes(); ?>&amp;url=<?php echo urlencode(the_permalink('','',false)); ?>" target="_blank" class="btn btn-default google"> <i class="fa fa-google-plus fa-lg google"></i></a>
             
                <a href="https://plus.google.com/share?url=<?php echo urlencode(the_permalink('','',false)); ?>&amp;title=<?php echo strip_tags($post->post_title); ?>" class="btn btn-default linkedin"><i class="fa fa-linkedin"></i></a>
              
                <a href="mailto:?subject=<?php echo strip_tags($post->post_title); ?>&amp;body=<?php echo urlencode(the_permalink('','',false)); ?>"> <i class="icomoon-envelope"></i> Email </a>
             
       </div>
        <!-- news sharing section end --> 
 
<?php 

if (get_theme_mod( 'hide_author_bio') == '') { ?>
<h4>Author's About yourself biography:</h4>
<p><?php the_author_meta('description'); ?></p>
<?php } 

if( get_theme_mod( 'hide_comments') == '') {
	// If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) {
        comments_template();
    }
}

?>
