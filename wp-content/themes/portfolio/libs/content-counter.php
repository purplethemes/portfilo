<?php
/**
 * The template for displaying counter
 *
 * @package Portfolio
 * @author Purplethemes
 */

?> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>


<?php

$cnt = 0;
for($i=1; $i<=4; $i++){
	if( get_theme_mod( 'counter_checkbox_'.$i) != '')
	{
		$cnt++;	
	}
}

switch($cnt){
    case '1':
		$class = 'col-xs-12 col-sm-6 col-md-12';
        break;
	case '2':
		$class = 'col-xs-12 col-sm-6 col-md-6';
		break;
	case '3':
		$class = 'col-xs-12 col-sm-6 col-md-4';
		break;
	case '4':
		$class = 'col-xs-12 col-sm-6 col-md-3';
		break;
    default :
        $class = '';
		break;
}
?>

<section class="counter-section">
    <div class="container">
     <?php if ($cnt != "0") { ?>
        <section class="row" id="counter-area">
           
            <?php for($i=1; $i<=4; $i++){ 
            
	            if( get_theme_mod( 'counter_checkbox_'.$i) != '' ) { ?>
	            
					<article class="<?php echo $class; ?>">
	                    <div class="counter-block wow fadeInUp" data-wow-delay="0.4s">
	                        <?php if(get_theme_mod( 'counter_image_'.$i) != '' && get_theme_mod( 'counter_checkbox_'.$i) != '' ){ ?><img src="<?php echo  get_theme_mod( 'counter_image_'.$i ); ?>" /><?php } else { echo ''; } ?>
	                        <?php if( get_theme_mod( 'counter_count_'.$i) != '' && get_theme_mod( 'counter_checkbox_'.$i) != '' ){ ?><p class="cnt-no"><?php echo get_theme_mod( 'counter_count_'.$i); ?></p><?php } else { echo ''; } ?>
	                        <?php if( get_theme_mod( 'counter_title_'.$i) != '' && get_theme_mod( 'counter_checkbox_'.$i) != '' ){ ?><p class="cnt-title"><?php echo get_theme_mod( 'counter_title_'.$i); ?></p><?php } else { echo ''; } ?>
	                    </div>
	                </article>	
				<?php }
            	
			} ?>
			
		</section>
		<?php } ?>
    </div>
</section>

