<?php
/*
	Customize Style
*/

$font_heading = get_theme_mod('font_heading', 'Open Sans');
$font_content = get_theme_mod('font_content', 'Open Sans');
?>

<link href='http://fonts.googleapis.com/css?family=<?php echo $font_heading; ?>|<?php echo $font_content; ?> ' rel='stylesheet' type='text/css'>

<style>

	body {
		background-color: <?php echo get_option('bodybg_color'); ?>
	}
	
	.navbar-inverse {
		background: <?php echo get_option('headerbg_color'); ?>
	}
	
	footer {
		background-color: <?php echo get_option('footerbg_color'); ?>
	}
	
	.nav {
		background-color: <?php echo get_option('menubg_color'); ?>
	}
	
	.custom-nav .navbar-nav > li > a, .custom-nav .navbar-nav > .current-menu-item > a {
		color: <?php echo get_option('menu_color'); ?>
	}
	
	.custom-nav .navbar-nav > .current-menu-item > a, .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .custom-nav .navbar-nav > .current-menu-item > a, .custom-nav .navbar-nav > .current-menu-item > a:hover, .custom-nav .navbar-nav > .current-menu-item > a:focus, .custom-nav .dropdown-menu > .current-menu-item > a, .custom-nav .dropdown-menu > .current-menu-item > a:hover, .custom-nav .dropdown-menu > .current-menu-item > a:focus {
		background-color: <?php echo get_option('active_menu_color'); ?>
	}
	
	.dropdown-menu > li > a:hover, .custom-nav .navbar-nav > li > a:hover,.custom-nav .navbar-nav > .current-menu-item > a, .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .custom-nav .navbar-nav > .current-menu-item > a, .custom-nav .navbar-nav > .current-menu-item > a:hover, .custom-nav .navbar-nav > .current-menu-item > a:focus, .custom-nav .dropdown-menu > .current-menu-item > a, .custom-nav .dropdown-menu > .current-menu-item > a:hover, .custom-nav .dropdown-menu > .current-menu-item > a:focus {
		color: <?php echo get_option('hover_menu_color'); ?>
	}
	
	.breadcrum-area {
		background: <?php echo get_option('breadcrumb_bg_color'); ?>
	}
	
	a, .service-container a, .company-info a, .more-news {
		color: <?php echo get_option('link_color'); ?>
	}
	
	
	
	.contact-info .info a:hover, .news-info h2 a:hover, .project-container h4 a:hover, .circle:hover, .footer-navigation .social-follow .socials li a:hover, .footer-navigation .copyright a:hover, .footer-navigation .copyright a:active, .footer-navigation .copyright a:focus, .sitemap ul li a:hover, a:hover, .breadcrum-area .breadcrum a:hover, .breadcrum-area .breadcrum a:active, .pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus, .breadcrum-area .breadcrum span.active, .back-to-top:hover, .sidebar ul li a:hover, .slider-banner h2 strong {
		color: <?php echo get_option('link_hover_color'); ?>
	}
    
    .plan-entry-hover a:hover, .back-to-top, .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus, .custom-nav .navbar-brand,.title-divider, .slider-banner .flex-control-paging li a.flex-active {
        background-color: <?php echo get_option('link_bg_color'); ?> !important;
    }
    
    h1, h2, h3, h4, h5, h6 {
		font-family: <?php echo $font_heading; ?> , sans-serif
	}
	
	 body {
		font-family: <?php echo $font_content; ?> , sans-serif
	}
</style>
	