<?php
/**
 * Theme functions.
 *
 * Portfoliotheme functions and definitions
 * @package Portfolio
 * @author Purplethemes
 */


/* ---------------------------------------------------------------------------
 * Theme support
 * --------------------------------------------------------------------------- */

if ( ! function_exists( 'portfolio_theme_setup' ) ) :


function portfolio_theme_setup() {
	
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 260, 146, false ); // admin - featured image
	add_image_size( '50x50', 50, 50, false ); // admin - lists
	
	add_image_size( 'portfolio-listing', 555, 415, true ); // portfolio - listing
	add_image_size( 'portfolio-single', 954, 440, true ); // portfolio - single
	add_image_size( 'portfolio-homepage', 240, 240, true ); // portfolio - homepage
	add_image_size( 'portfolio-homepagelarge', 799, 539, true ); //portfolio - homepage (large)
	
    add_image_size( 'gallery-listing', 555, 415, true ); // gallery - listing
    add_image_size( 'gallery-homepage', 240, 240, true ); // gallery - homepage
	add_image_size( 'galllery-homepagelarge', 799, 539, true ); //gallery - homepage (large)
	
	add_image_size( 'cpt-logo-thumbnail',100,100 );
	
	add_image_size( 'aboutus-thumbnail', 1140 ,640,true);
	
	add_image_size( 'blog-listing-thumbnail', 458 ,244,true);
	add_image_size( 'blog-detail-thumbnail', 750 ,400,true);
	
	add_image_size( 'testimonial-homepage', 87, 88, true ); // testimonial - homepage
    //add_image_size( 'pf-homepage', 253 ,253);
}
endif; // portfolio_theme_setup

add_action( 'after_setup_theme', 'portfolio_theme_setup' );


/***************** Pagination function *****************/    

if ( ! function_exists( 'wpt_pagenum_link' ) ) :

//add_filter( 'get_pagenum_link', 'wpt_pagenum_link' );

function wpt_pagenum_link( $link )
{
    return preg_replace( '~/page/(\d+)/?~', '?paged=\1', $link );
}
endif; // wpt_pagenum_link



if ( ! function_exists( 'wpt_pagination' ) ):  
function wpt_pagination() {
   
	global $wp_query;
	$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;  
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	if( empty( $paged ) ) $paged = 1;
	$prev = $paged - 1;							
	$next = $paged + 1;
	
	$end_size = 1;
	$mid_size = 2;
	$show_all = true;
	$dots = false;	
	if( ! $total = $wp_query->max_num_pages ) $total = 1;
	
	if( $total > 1 )
	{
		
		if( $paged >1 ){
			echo '<li><a class="prev_page" href="'. get_pagenum_link($current-1) .'">'. __('&lsaquo;','wpt') .'</a></li>';
		}

		for( $i=1; $i <= $total; $i++ ){
			if ( $i == $current ){
				echo '<li class="active">';
					echo '<a href="'. get_pagenum_link($i) .'">'. $i .'</a>&nbsp;';
				echo '</li>';
				$dots = true;
			} else {
				if ( $show_all || ( $i <= $end_size || ( $current && $i >= $current - $mid_size && $i <= $current + $mid_size ) || $i > $total - $end_size ) ){
					echo '<li>';
					   echo '<a href="'. get_pagenum_link($i) .'">'. $i .'</a>&nbsp;';
				    echo '</li>';
					$dots = true;
				} elseif ( $dots && ! $show_all ) {
					echo '<span class="page">...</span>&nbsp;';
					$dots = false;
				}
			}
		}
		
		if( $paged < $total ){
			echo '<li><a class="next_page" href="'. get_pagenum_link($page+1) .'">'. __('&rsaquo;','wpt') .'</a></li>';
		}

	}	
}
endif; //wpt_pagination

/*------------------------------------------------
	check for previous posts
------------------------------------------------ */
if ( ! function_exists( 'issuetheme_has_previous_posts' ) ) :
function issuetheme_has_previous_posts() {
	ob_start();
	previous_posts_link();
	$result = strlen(ob_get_contents());
	ob_end_clean();
	return $result;
}
endif; //issuetheme_has_previous_posts

/*------------------------------------------------
	check for next posts
------------------------------------------------ */
if ( ! function_exists( 'issuetheme_has_next_posts' ) ) :
function issuetheme_has_next_posts() {
	ob_start();
	next_posts_link();
	$result = strlen(ob_get_contents());
	ob_end_clean();
	return $result;
}
endif; //issuetheme_has_next_posts

/***************** Breadcrumbs function *****************/  
if ( ! function_exists( 'wpt_breadcrumbs' ) ):  
function wpt_breadcrumbs() {

global $post,$wp_query;

$homeLink = home_url();
$showCurrent = 1;
	
    echo '<span data-wow-delay="1s" class="breadcrum pull-left wow fadeIn animated" style="visibility: visible; animation-delay: 1s; animation-name: fadeIn;">';
	    echo '<ul>';
        	echo '<li><a href="'. $homeLink .'"><i class="glyphicon glyphicon-home"></i></a> <span class="breadcrum-sep">/</span></li>';

                 // Blog Category
        	    if ( is_category() ) {
        		    echo '<li><span class="active">' . __('Archive by category','wpt').' "' . single_cat_title('', false) . '"</span></li>';
                 }
        	    // Blog Day
        	    
                elseif ( is_day() ) {
        		    echo '<li><a href="'. get_year_link($wp_query->query_vars['year']) . '">'. $wp_query->query_vars['year'] .'</a> <span class="breadcrum-sep">/</span></li>';
        		    echo '<li><a href="'. get_month_link($wp_query->query_vars['year'],$wp_query->query_vars['monthnum']) .'">'. date_i18n("F", mktime(0, 0, 0, $wp_query->query_vars['monthnum'], 10)) .'</a> <span class="breadcrum-sep">/</span></li>';
        		    echo '<li><span class="active">'. $wp_query->query_vars['day'] .'</span></li>';
                } 
        	    // Blog Month
        	    
                elseif ( is_month() ) {
                	
        		    echo '<li><a href="' . get_year_link($wp_query->query_vars['year']) . '">' .$wp_query->query_vars['year'] . '</a> <span class="breadcrum-sep">/</span></li>';
        		    echo '<li><span class="active">'. date_i18n("F", mktime(0, 0, 0, $wp_query->query_vars['monthnum'], 10)) .'</span></li>';   
                }
        	    // Blog Year
        	     elseif ( is_year() ) {
        		    
        		    echo '<li><span class="active">'. $wp_query->query_vars['year'] .'</span></li>';
                }
                
                elseif (  get_post_type() == 'post'  ) {
				      $post_for_page_id =get_option('page_for_posts');
				      echo '<li><a href="' . get_page_link( $post_for_page_id ) . '">'. get_the_title( $post_for_page_id ) .'</a></li>';
				      
                }

        	    // Single Post
        	     elseif ( is_single() && !is_attachment() ) {
        	     	
        		// Custom post type
        		if ( get_post_type() != 'post' ) {
        			global $wpdb;
        			$post_type = get_post_type_object(get_post_type());
        			$slug = $post_type->rewrite;
        			
        			if( $slug['slug'] == 'portfolio'  && $portfolio_page_id = get_option('portfolio-page') ){	
                        echo '<li><a href="' . get_page_link( $portfolio_page_id ) . '">'. get_the_title( $portfolio_page_id ) .'</a> <span class="breadcrum-sep">/</span></li>';
					} else {
						echo '<li><a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> <span class="breadcrum-sep">/</span></li>';
					} 
        			if ($showCurrent == 1) 
        			   echo '<li><span class="active">'. get_the_title() .'</span></li>';
        		}
                // Blog post
                else {
        			$cat = get_the_category(); 
        			$cat = $cat[0];
                   
        			echo '<li>';
        				echo get_category_parents($cat, TRUE, ' <span class="breadcrum-sep">/</span>');
        			echo '</li>';
        			echo '<li><span class="active">'. wp_title( '',false ) .'</span></li>';
        		}

        	    // Taxonomy
        	    } 
                elseif( get_post_taxonomies() ) {
        		    global $wpdb;
        		    $post_type = get_post_type_object(get_post_type());
        		    $slug = $post_type->rewrite;
        		    
        		    if( $post_type->name == 'portfolio' && $portfolio_page_id = get_option('portfolio-page') ){	
                        echo '<li><a href="' . get_page_link( $portfolio_page_id ) . '">'. get_the_title( $portfolio_page_id ) .'</a> <span class="breadcrum-sep">/</span></li>';
					} else {
						echo '<li><a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> <span class="breadcrum-sep">/</span></li>';
                    }
                    if(is_tax() ){
                        $terms_object = get_queried_object();
                        echo '<li><span class="active">'. $terms_object->name .'</span></li>';
                    }
        	    } 
                // Page with parent
                elseif ( is_page() && $post->post_parent ) {
        		    $parent_id  = $post->post_parent;
        		    $breadcrumbs = array();
        		    while ($parent_id) {
            			$page = get_page($parent_id);
            			$breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a> <span class="breadcrum-sep">/</span></li>';
            			$parent_id  = $page->post_parent;
                        
        		    }
        		
                    $breadcrumbs = array_reverse($breadcrumbs);
                    for ($i = 0; $i < count($breadcrumbs); $i++) {
                        echo $breadcrumbs[$i];
                    }
                    
                  if ($showCurrent == 1) echo '<li><span class="active">'. get_the_title() .'</span></li>';
              
        	    } 
                // Default
                else {
        		    echo '<li><span class="active">'. get_the_title() .'</span></li>';
        	    }
        echo '</ul>';
	echo '</span>';
}
endif; //wpt_breadcrumbs
   


if ( ! function_exists( 'wpt_taxonomy_slug_rewrite' ) ) :
/*
 * Replace Taxonomy slug with Post Type slug in url
 * Version: 1.1
 */
 
function wpt_taxonomy_slug_rewrite($wp_rewrite) {
	
    $rules = array();
    // get all custom taxonomies
    $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
    // get all custom post types
    $post_types = get_post_types(array('public' => true, '_builtin' => false), 'objects');
    
    
    foreach ($post_types as $post_type) {
        foreach ($taxonomies as $taxonomy) {
         
            // go through all post types which this taxonomy is assigned to
            foreach ($taxonomy->object_type as $object_type) {
                 
                // check if taxonomy is registered for this custom type
                if ($object_type == $post_type->rewrite['slug']) {
             
                    // get category objects
                    $terms = get_categories(array('type' => $object_type, 'taxonomy' => $taxonomy->name, 'hide_empty' => 0));
             
                    // make rules
                    foreach ($terms as $term) {
                        $rules[$object_type . '/' . $term->slug . '/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug;
                    }
                }
            }
        }
    }
    // merge with global rules
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
   
}
endif; // wpt_taxonomy_slug_rewrite

//add_filter('generate_rewrite_rules', 'wpt_taxonomy_slug_rewrite');


/* ---------------------------------------------------------------------------
*	TGM Plugin Activation
* --------------------------------------------------------------------------- */

if ( ! function_exists( 'wpt_register_required_plugins' ) ) :

function wpt_register_required_plugins() {

	$plugins = array(
			array(
					'name'     				=> 'Responsive Contact Form', // The plugin name
					'slug'     				=> 'responsive-contact-form', // The plugin slug (typically the folder name)
					'source'   				=> THEME_DIR. '/functions/plugins/responsive-contact-form.zip', // The plugin source
					'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
					'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
					'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
					'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
					'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			
			array(
					'name'     				=> 'Responsive Contact Form Mailchimp Extension', // The plugin name
					'slug'     				=> 'responsive-contact-form-mailchimp-extension', // The plugin slug (typically the folder name)
					'source'   				=> THEME_DIR. '/functions/plugins/responsive-contact-form-mailchimp-extension.zip', // The plugin source
					'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
					'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
					'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
					'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
					'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			
			array(
					'name'     				=> 'Swift Custom Post Navigator', // The plugin name
					'slug'     				=> 'swift-custom-post-navigator', // The plugin slug (typically the folder name)
					'source'   				=> THEME_DIR. '/functions/plugins/swift-custom-post-navigator.zip', // The plugin source
					'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
					'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
					'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
					'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
					'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			
	);

	// Change this to your theme text domain, used for internationalising strings
	$theme_text_domain = 'wpt';

	$config = array(
			    'default_path' => '', // Default absolute path to pre-packaged plugins.
				'menu' => 'tgmpa-install-plugins', // Menu slug.
				'has_notices' => true, // Show admin notices or not.
				'dismissable' => true, // If false, a user cannot dismiss the nag message.
				'dismiss_msg' => '', // If 'dismissable' is false, this message will be output at top of nag.
				'is_automatic' => false, // Automatically activate plugins after installation or not.
				'message' => '', // Message to output right before the plugins table.
				'strings' => array(
				'page_title' => __( 'Install Required Plugins', 'tgmpa' ),
				'menu_title' => __( 'Install Plugins', 'tgmpa' ),
				'installing' => __( 'Installing Plugin: %s', 'tgmpa' ), // %s = plugin name.
				'oops' => __( 'Something went wrong with the plugin API.', 'tgmpa' ),
				'notice_can_install_required' => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s).
				'notice_can_install_recommended' => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s).
				'notice_cannot_install' => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s).
				'notice_can_activate_required' => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s).
				'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s).
				'notice_cannot_activate' => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s).
				'notice_ask_to_update' => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s).
				'notice_cannot_update' => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s).
				'install_link' => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
				'activate_link' => _n_noop( 'Begin activating plugin', 'Begin activating plugins' ),
				'return' => __( 'Return to Required Plugins Installer', 'tgmpa' ),
				'plugin_activated' => __( 'Plugin activated successfully.', 'tgmpa' ),
				'complete' => __( 'All plugins installed and activated successfully. %s', 'tgmpa' ), // %s = dashboard link.
				'nag_type' => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
				)
				);
				tgmpa( $plugins, $config );

}
endif; // wpt_register_required_plugins
add_action( 'tgmpa_register', 'wpt_register_required_plugins' );

/* ---------------------------------------------------------------------------
 * Excerpt
* --------------------------------------------------------------------------- */

if ( ! function_exists( 'wpt_excerpt' ) ) :

function wpt_excerpt($post, $length = 180, $tags_to_keep = '<a><em><strong>', $extra = '') {
		
	if(is_int($post)) {
		$post = get_post($post);
	} elseif(!is_object($post)) {
		return false;
	}

	if(has_excerpt($post->ID)) {
		$the_excerpt = $post->post_excerpt;
		return apply_filters('the_content', $the_excerpt);
	} else {
		$the_excerpt = $post->post_content;
	}
	
	$the_excerpt = strip_shortcodes(strip_tags($the_excerpt, $tags_to_keep));
	$the_excerpt = preg_split('/\b/', $the_excerpt, $length * 2+1);
	$excerpt_waste = array_pop($the_excerpt);
	$the_excerpt = implode($the_excerpt);
	$the_excerpt .= $extra;
	
	return apply_filters('the_content', $the_excerpt);
}
endif; //wpt_excerpt

/* ---------------------------------------------------------------------------
 * Excerpt length
 * --------------------------------------------------------------------------- */
 
if ( ! function_exists( 'wpt_excerpt_length' ) ) : 
 
function wpt_excerpt_length( $length ) {
	return 40;
}
endif; //wpt_excerpt_length
add_filter( 'excerpt_length', 'wpt_excerpt_length', 999 );


// custom excerpt ellipses for 2.9+
if ( ! function_exists( 'wpt_custom_excerpt_more' ) ) : 

function wpt_custom_excerpt_more($more) {
	return '...';
}
endif; //wpt_custom_excerpt_more
add_filter('excerpt_more', 'wpt_custom_excerpt_more');

/* custom excerpt ellipses for 2.8-
function custom_excerpt_more($excerpt) {
	return str_replace('[...]', '...', $excerpt);
}
add_filter('wp_trim_excerpt', 'wpt_custom_excerpt_more'); 
*/
?>