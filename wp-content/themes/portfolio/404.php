<?php
/**
 * The template for displaying 404 pages (Not Found)
 * Portfoliotheme functions and definitions
 * @package Portfolio
 * @author Purplethemes
*/

get_header(); ?>

<!-- breadcrum section start -->
    <section class="breadcrum-area">
        <div class="container">
            <h2 data-wow-delay="0.7s" class="wow fadeIn animated pull-right" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeIn;">Page Not Found</h2>
            <span data-wow-delay="1s" class="breadcrum pull-left wow fadeIn animated" style="visibility: visible; animation-delay: 1s; animation-name: fadeIn;"><a href="<?php echo site_url(); ?>"><i class="glyphicon glyphicon-home"></i></a>/&nbsp;&nbsp;<span class="active">Page Not Found</span></span>
        </div>
    </section>
<!-- breadcrum section end -->

<!-- 404 section -->
  <div class="container">
    <section class="about-section page-not">
      <div class="title-area wow fadeIn">
        <div class="page-not-found text-center">
            <i class="fa fa-warning fa-2x"></i>
        	<span>404</span>
        </div>
        <p class="section-sub-text">Sorry, It appears the page you were looking for doesn't exist anymore or might have been moved.<br/>Would you like to go to <a href="<?php echo site_url(); ?>">homepage</a> instead?</p>
      </div>      
    </section>
  </div>
<!-- 404 section end -->

<?php get_footer(); ?>
