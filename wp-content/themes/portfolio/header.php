<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * Portfoliotheme functions and definitions
 * @package Portfolio
 * @author Purplethemes
 */
 
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <title><?php wp_title('|', true, 'right'); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="keywords" content="<?php bloginfo('name'); ?>">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="author" content="<?php bloginfo('name'); ?>">
    
    <!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	
	 <!-- stylesheet -->
	<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" media="all" />
	<?php do_action('wp_styles'); ?>
	
    <!-- favicon section -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo (get_theme_mod( 'wpt_favicon' ) != '') ? get_theme_mod( 'wpt_favicon' ) : THEME_URI.'/ico/favicon.png';?>" />
    <!-- favicon section end -->
    <?php do_action('wp_enqueue_scripts'); ?>
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php 

	get_template_part( 'libs/header', 'top-area' );
    
    $slider = false;
    if( get_post_type()=='page' && is_front_page() ) {
    	get_template_part( 'libs/header', 'slider' ) ;
    	$slider = true;
    }	
    if( get_theme_mod( 'hide_breadcrumbs_from_bar') == '' ){
        if( ! is_404() && $slider != 1 ){  
            if( trim( wp_title( '', false ) ) ){
          	// Page title
          	    echo '<section class="breadcrum-area">';
                    echo '<div class="container">';                    
                           if( get_post_type()=='page' || is_single() ){
                           	
						    echo '<h2 data-wow-delay="0.7s" class="wow fadeIn animated pull-right" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeIn;">'. $post->post_title .'</h2>';
				       } 
                           else if( is_tax() ) {
                           
                                 $queried_object = get_queried_object(); 
                                 $term_id = $queried_object->term_id; 
                                 $term_obj = get_term( $term_id, "portfolio_category"); 
                                 echo '<h2 data-wow-delay="0.7s" class="wow fadeIn animated pull-right" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeIn;">'. $term_obj->name .'</h2>';
                           }
                           
                           else if( get_post_type()=='post' ) {
						   	$pagename = get_query_var('pagename');
						   	$post = $wp_query->get_queried_object();
							$pagename = $post->post_name;
							echo '<h2 data-wow-delay="0.7s" class="wow fadeIn animated pull-right" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeIn;">'. $pagename .'</h2>';

						   }
                           else {
                           
						    echo '<h2 data-wow-delay="0.7s" class="wow fadeIn animated pull-right" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeIn;">'. trim( wp_title( '', false ) ) .'</h2>';
				        }
                         
                              	 wpt_breadcrumbs();
                              
                    echo '</div>';
               echo '</section>';
            }
        }  
    }//end of if    

?>
