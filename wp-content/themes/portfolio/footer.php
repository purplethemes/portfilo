<?php
/**
 * The template for displaying the footer.
 *
 * @package Portfolio
 * @author August Infotech
 */
?>
<!-- footer section -->
<footer>
        <section class="footer-navigation">
            <div class="container">
                <section class="row">
                    <article class="col-xs-12 col-sm-6 col-md-3">

                        <!-- footer social section -->
                        <div class="social-follow wow fadeInUp">
                            <h5 class="text-center"><?php _e('Follow Us On','wpt'); ?></h5>
                            <ul class="socials">
                            
                            <?php 
                           
                            if( get_theme_mod('twitter_url') )
                            	echo '<li><a class="ir" href="'.get_theme_mod( 'twitter_url').'" target="_blank" title="'. __('Share on Twitter','wpt') .'"><i class="fa fa-twitter"></i></a></li>'; 
                            	
                           	if( get_theme_mod( 'facebook_url' ) )
                           		echo '<li><a class="ir" href="'.get_theme_mod( 'facebook_url' ).'" target="_blank" title="'. __('Share on Facebook','wpt') .'"><i class="fa fa-facebook"></i></a></li>';
                           		
                           	if( get_theme_mod( 'googleplus_url' ) )
                           		echo '<li><a class="ir" href="'.get_theme_mod( 'googleplus_url' ).'" target="_blank" title="'. __('Share on Google Plus','wpt') .'"><i class="fa fa-google-plus"></i></a></li>';
                           		
                           	if( get_theme_mod( 'linkedin_url' ) )
                           		echo '<li><a class="ir" href="'.get_theme_mod( 'linkedin_url' ).'" target="_blank" title="'. __('Share on LinkedIn','wpt') .'"><i class="fa fa-linkedin"></i></a></li>';
                           	
                           	if( get_theme_mod( 'pinterest_url' ) )
                           		echo ' <li><a class="ir" href="'.get_theme_mod( 'pinterest_url' ).'" target="_blank" title="'. __('Link to your Pinterest profile','wpt') .'"><i class="fa fa-pinterest"></i></a></li>';
                                
                           		
                           	if( get_theme_mod( 'blogger_url' ) )
                           		echo '<li><a class="ir" href="'.get_theme_mod( 'blogger_url' ).'" target="_blank" title="'. __('Share on RSS','wpt'). '"><i class="fa fa-rss"></i></a></li>';
                           		
                           	if( get_theme_mod( 'youtube_url' ) )
                           		echo ' <li><a class="ir" href="'.get_theme_mod( 'youtube_url' ).'" target="_blank" title="'. __('Share on YouTube','wpt') .'"><i class="fa fa-youtube"></i></a></li>';
                           		
                           	if( get_theme_mod( 'flikr_url' ) )
                           		echo '<li><a class="ir" href="'.get_theme_mod( 'flikr_url' ).'" target="_blank" title="'. __('Share on Flickr','wpt') .'"><i class="fa fa-flickr"></i></a></li>';
                           		
                           	if( get_theme_mod( 'vimeo_url' ) )
                           		echo '<li><a class="ir" href="'.get_theme_mod( 'vimeo_url' ).'" target="_blank" title="'. __('Share on Vimeo','wpt') .'"><i class="fa fa-vimeo-square"></i></a></li>';
                            
                            ?>   
                            </ul>
                        </div>
                        <!-- footer social section end -->

                    </article>
                    <article class="col-xs-12 col-sm-6 col-md-3">

                        <!-- newsletter section -->
                        <div class="newsletter wow fadeInUp" data-wow-delay="0.3s">
                            <h5 class="text-center"><?php _e('Newsletter','wpt'); ?></h5>
                            <form id="newsletter_form" method="post">
                                <div class="subscribe-block">
                                    <div class="input-box">
                                        <input type="email" name="email" id="newsletter" class="input-text required email" placeholder="<?php _e('Email Address','wpt'); ?>" required autocomplete="off">
                                    </div>
                                    <div class="actions">
                                        <button type="submit" title="<?php _e('Go','wpt'); ?>" class="button"><span><span><i class="fa fa-check"></i></span></span>
                                        </button>
                                        
                                   </div>
                                </div>
                            </form>
                            <script type="text/javascript">
                            	var Wpt_Ajax = "<?php echo home_url(); ?>/wp-admin/admin-ajax.php";
                            </script>
                        </div>
                        <!-- newsletter section end -->

                    </article>
                    <article class="col-xs-12 col-sm-12 col-md-6">
                        <!-- sitemap section -->
                        <div class="sitemap-block wow fadeInUp" data-wow-delay="0.4s">
                            <?php wpt_wp_footer_menu(); ?>
                        </div>
                    </article>
                     <!-- sitemap section end -->
                    <article class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <!-- copyright section -->
                       
                        <div class="copyright"> <?php if ( get_theme_mod( 'footer_text' ) ) : ?>&copy; <?php echo get_theme_mod( 'footer_text' ); ?> <?php else : ?><?php _e('&copy; 2015. All rights reserved','wpt'); ?> <?php endif; ?><?php _e('| Design by','wpt'); ?> <a href="http://www.augustinfotech.com/" target="_blank"><?php _e('August Infotech','wpt'); ?></a>.
                        </div>
                        <!-- copyright section end -->
                    </article>
                </section>
            </div>
        </section>
    </footer>
    <!-- footer section end-->
    
    <!-- scroll to top section -->
    <a href="#" class="back-to-top"><i class="glyphicon glyphicon-arrow-up"></i></a> 
    <!-- scroll to top section end -->
	<!-- main body section end -->
	
<!-- wp_footer() -->
<!-- included javascript section -->	
<?php wp_footer(); ?>	
</body>
</html>