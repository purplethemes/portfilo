<?php
/**
 * The Blog Sidebar containing the widget area.
 *
 * Portfoliotheme functions and definitions
 * @package Portfolio
 * @author Purplethemes
 */
 

?>

<div style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight;" class="popular wow fadeInRight animated animated" data-wow-delay="0.3s">
	
    <?php dynamic_sidebar( 'sidebar-2' ); ?>
</div>