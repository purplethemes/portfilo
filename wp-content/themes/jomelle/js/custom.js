jQuery(document).ready(function($) {
    "use strict";

    $('body.preloader').jpreLoader({
        showSplash : false,
        loaderVPos : '50%',
    }).css('visibility','visible');

                        
    $(".scroll").click(function(e){
        e.preventDefault();
        var scroll_to = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(scroll_to).offset().top-20
        }, 1000);
    });


    $('#jomelle_back_to_top').click(function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, 500);
        return false;
    })


    $('.home.page .dnd_section_DD').waypoint(function(direction) {
        var section_id = $(this).attr('id');
        if(section_id!==undefined){
            $('.current-menu-item, .current-menu-ancestor').removeClass('current-menu-item').removeClass('current-menu-ancestor');
            if(direction==='down'){
                var $menu_item = $('#main_menu a[href=#'+section_id+']').parent();
                if($menu_item.length>0){
                    $menu_item.addClass('current-menu-item');
                }
                else{
                    $('#main_menu .current_page_item').addClass('current-menu-item');
                }
            }
            else if(direction==='up'){
                var previous_section_id = $(this).prevAll('[id]:first').attr('id');
                var $menu_item = $('#main_menu a[href=#'+previous_section_id+']').parent();
                if($menu_item.length>0){
                    $menu_item.addClass('current-menu-item');
                }
                else{
                    $('#main_menu .current_page_item').addClass('current-menu-item');
                }
            }
        }
    },{
      offset: 100
    });



    var $main_slider = $('#Abdev_main_slider');
    $main_slider.height('auto');
    var $main_header = $('#Abdev_main_header');
    var $header_spacer = $('#Abdev_header_spacer');

    $('#Abdev_main_slider.Abdev_parallax_slider').height($(window).height());

    var header_height = $main_header.outerHeight();

    $header_spacer.height(header_height).hide();
    var admin_toolbar_height = parseInt($('html').css('marginTop'), 10);

    function sticky_header(){
        if($(window).width()>767){
            $main_header.removeClass('sticky_header').removeClass('sticky_header_low');
            $main_header.css({
                'z-index': '2000',
                'position': 'static'
            });
            $main_header.css('position','fixed').css('top', 0+admin_toolbar_height).addClass('sticky_header');
            $header_spacer.show();
            $(document).scroll(function(){
                var padding_diff = parseInt($(document).scrollTop()/2 ,10);
                padding_diff = (padding_diff > 20) ? 20 : padding_diff;
                if(padding_diff>19){
                    $main_header.addClass('sticky_header_low');
                }
                else{
                    $main_header.removeClass('sticky_header_low');
                }
                $header_spacer.height(header_height - padding_diff*2);
                $main_header.css({'paddingTop':30-padding_diff , 'paddingBottom':30-padding_diff});
            });
        }
        else{
            $header_spacer.hide();
            $main_header.css({
                'position':'relative',
                'top': 0,
                'padding-top': '30px',
                'padding-bottom': '30px',
                'z-index': '0'
            }).removeClass('sticky_header').removeClass('sticky_header_low');
        }
    }
    //sticky_header();

    // Search toggle
    $('.search-icon').on('click', function(e) {
        e.preventDefault();
        var $that = $(this);
        var $wrapper = $('.search-box-wrapper');

        $that.toggleClass('active');
        $wrapper.slideToggle('300');

        if ($that.hasClass('active')) {
            $wrapper.find('input').focus();
        }
    } );
    

    $('.accordion-group').on('show', function() {
        $(this).find('i').removeClass('icon-plus').addClass('icon-minus');
    });
    $('.accordion-group').on('hide', function() {
        $(this).find('i').removeClass('icon-minus').addClass('icon-plus');
    });


    var sf, body;
    body = $('body');
    sf = $('#main_menu');
    if($('#ABdev_menu_toggle').css('display') === 'none') {
        // enable superfish when the page first loads if we're on desktop
        sf.superfish({
            delay:          300,
            animation:      {opacity:'show',height:'show'},
            animationOut:   {height:'hide'},
            speed:          'fast',
            speedOut:       'fast',            
            cssArrows:      false, 
            disableHI:      true /* load hoverIntent.js in header to use this option */,
            onBeforeShow:   function(){
                var ww = $(window).width();
                var locUL = this.parent().offset().left + this.width();
                var locsubUL = this.parent().offset().left + this.parent().width() + this.width();
                var par = this.parent();
                if(par.hasClass("menu-item-depth-0") && (locUL > ww)){
                    this.css('marginLeft', "-"+(locUL-ww+20)+"px");
                }
                else if (!par.hasClass("menu-item-depth-0") && (locsubUL > ww)){
                    this.css('left', "-"+(this.width())+"px"); 
                }
            }
        });
    }

    /*********** Progress Bar Vertical ************************************************************/
       if(!jQuery.browser.mobile){
        $(".dnd_progress_bar_vertical .dnd_meter_vertical .dnd_meter_percentage_vertical").height(0).one('inview', function(event, isInView) {
          if (isInView) {
            var newheight = $(this).data("percentage") + '%';
            $(this).animate({height: newheight}, {
                duration:1500,
                step: function(now) {
                    $(this).find('span').html(Math.floor(now) + '%');
                    var above_tenths = Math.floor(now/10);
                    for(var i=1; i<=above_tenths; i++){
                        $(this).addClass('dnd_meter_above'+above_tenths*10);
                    }
                }
            });
          }
        });
    }
    else{
        $(".dnd_progress_bar_vertical .dnd_meter_vertical .dnd_meter_percentage_vertical").each(function(){
            var newheight = $(this).data("percentage");
            $(this).css('height', newheight+'%');
            for(var i=0; i<=newheight; i++){
                var above_tenths = Math.floor(i/10);
                $(this).addClass('dnd_meter_above'+above_tenths*10);
            }

        });
    }


    var $menu_responsive = $('#Abdev_main_header nav');
    $('#ABdev_menu_toggle').click(function(){
        $menu_responsive.animate({width:'toggle'},350);
    });


     $('.dnd-tabs-timeline').each(function(){
        var $this = $(this);
        var $tabs = $this.find('.dnd-tabs-ul > li');
        var tabsCount = $tabs.length;
        $tabs.addClass('tab_par_'+tabsCount);
    });


    $(".fancybox").fancybox({
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic',
        'titlePosition'     : 'over',
        'cyclic'            : true,
        'overlayShow'       : true,
        'titleFormat'       : function(title, currentArray, currentIndex) {
            return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
        }
    });
    

    $("footer .ABss_form_wrapper .ABss_inline_form .ABss_subscriber_email").parent().append('<a href="#" class="ABss_subscriber_widget_submit submit"><i class="ci_icon-chevron-right"></i></a>');
    
    $('input, textarea').placeholder();

    $(".submit").click(function (e) {
        e.preventDefault();
        $(this).closest("form").submit();
    });

    
    var $content = $("#timeline_posts");
    var $loader = $("#timeline_loading");
    var itemSelector = ('.timeline_post');
    function Timeline_Classes(){ 
        $content.find(itemSelector).each(function(){
           var posLeft = $(this).css("left");
           if(posLeft == "0px"){
               $(this).removeClass('timeline_post_right').addClass('timeline_post_left');          
           }
           else{
               $(this).removeClass('timeline_post_left').addClass('timeline_post_right');
           } 
        });
    }
    $content.imagesLoaded( function() {
        $content.masonry({
          columnWidth: ".timeline_post_first",
          gutter: 100,
          itemSelector: itemSelector,
        });
        Timeline_Classes();
        $loader.bind('inview', function(event, isInView) {
          if (isInView) {
            pageNumber++;
            load_posts();
          }
        });
    });
    var pageNumber = 1;
    var load_posts = function(){
            var url = $loader.data("ajaxurl") + '&pageNumber=' + pageNumber;
            var noPosts = $loader.data("noposts");
            $.ajax({
                type       : "GET",
                dataType   : "html",
                url        : url,
                beforeSend : function(){
                        $loader.addClass('timeline_loading_loader').html('');
                },
                success : function(data){
                    var $data = $(data);
                    if($data.length){
                            var $newElements = $data.css({ opacity: 0 });
                            $content.append( $newElements );
                        $content.imagesLoaded(function(){
                            $loader.removeClass('timeline_loading_loader');
                            $content.masonry( 'appended', $newElements, false );
                            $newElements.animate({ opacity: 1 });
                            Timeline_Classes();
                        }); 
                    } else {
                        $loader.removeClass('timeline_loading_loader').html(noPosts);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                },
                complete : function(){
                    Timeline_Classes();
                }
        });
    };



    var $isotope_container = $('#Abdev_latest_portfolio');
    $isotope_container.imagesLoaded( function() {
        $isotope_container.isotope({
            itemSelector : '.portfolio_item',
            animationEngine: 'best-available',
        });
        var $optionSets = $('.option-set'),
            $optionLinks = $optionSets.find('a');
        $optionLinks.click(function(){
            var $this = $(this);
            if ( $this.hasClass('selected') ) {
                return false;
            }
            var $optionSet = $this.parents('.option-set');
            $optionSet.find('.selected').removeClass('selected');
            $this.addClass('selected');
            var options = {},
                key = $optionSet.attr('data-option-key'),
                value = $this.attr('data-option-value');
            value = value === 'false' ? false : value;
            options[ key ] = value;
            if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
                changeLayoutMode( $this, options );
            } else {
                $isotope_container.isotope( options );
            }
            return false;
        });
    });

    /*Nivo Slider in Portfolio*/

    $(window).load(function() {
        $('#slider').nivoSlider({
            effect:'fade', // Specify sets like: 'fold,fade,sliceDown' 
            pauseTime:3000, // How long each slide will show
            directionNav:false, // Next & Prev navigation
            controlNavThumbs:true,
            controlNavThumbsFromRel:false,
            manualAdvance: false,
        });
    });


    $(window).resize(function() {

        Timeline_Classes();

        //sticky_header();
        
        $('#Abdev_latest_portfolio').isotope('reLayout');

        if($('#ABdev_menu_toggle').css('display') === 'none' && !sf.hasClass('sf-js-enabled')) {
            // you only want SuperFish to be re-enabled once (sf.hasClass)
            $menu_responsive.show();
            sf.superfish({
                delay:          300,
                animation:      {opacity:'show',height:'show'},
                animationOut:   {height:'hide'},
                speed:          'fast',
                speedOut:       'fast',            
                cssArrows:      false, 
                disableHI:      true /* load hoverIntent.js in header to use this option */,
                onBeforeShow:   function(){
                    var ww = $(window).width();
                    var locUL = this.parent().offset().left + this.width();
                    var locsubUL = this.parent().offset().left + this.parent().width() + this.width();
                    var par = this.parent();
                    if(par.hasClass("menu-item-depth-0") && (locUL > ww)){
                        this.css('marginLeft', "-"+(locUL-ww+20)+"px");
                    }
                    else if (!par.hasClass("menu-item-depth-0") && (locsubUL > ww)){
                        this.css('left', "-"+(this.width())+"px"); 
                    }
                }
            });
        } else if($('#ABdev_menu_toggle').css('display') != 'none' && sf.hasClass('sf-js-enabled')) {
            // smaller screen, disable SuperFish
            sf.superfish('destroy');
            $menu_responsive.hide();
        }
    });
    

});


