<?php 
global $jomelle_options;
?>
	<footer id="Abdev_main_footer">

		<?php if(is_active_sidebar('First Footer Widget') || is_active_sidebar('Second Footer Widget') || is_active_sidebar('Third Footer Widget') || is_active_sidebar('Fourth Footer Widget')): ?>
			<div id="footer_columns">
				<div class="container">
					<div class="row">
						<div class="span3 clearfix">
							<?php dynamic_sidebar( __('First Footer Widget', 'ABdev_jomelle') );?>
						</div>
						<div class="span3 clearfix">
							<?php dynamic_sidebar( __('Second Footer Widget', 'ABdev_jomelle') );?>
						</div>
						<div class="span3 clearfix">
							<?php dynamic_sidebar( __('Third Footer Widget', 'ABdev_jomelle') );?>
						</div>
						<div class="span3 clearfix">
							<?php dynamic_sidebar( __('Fourth Footer Widget', 'ABdev_jomelle') );?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div id="footer_copyright">
			<div class="container">
				<div class="row">
					<div class="span5 footer_copyright">
						<?php echo (isset($jomelle_options['copyright'])) ? $jomelle_options['copyright']: ''; ?>	 
					</div>
					<div class="span2 aligncenter"><a href="#" id="jomelle_back_to_top" title="<?php _e('Scroll to top', 'ABdev_jomelle'); ?>"></a></div>
					<div class="span5 footer_social">
						<?php 
						$target = (isset($jomelle_options['footer_social_target'])) ? $jomelle_options['footer_social_target'] : '_blank';
						?>
						<?php if(isset($jomelle_options['footer_linkedin_url']) && $jomelle_options['footer_linkedin_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_linkedin_url'];?>" class="dnd_tooltip" title="<?php _e('Follow us on Linkedin','ABdev_jomelle'); ?>" data-gravity="s" target="<?php echo $target; ?>"><i class="ci_icon-linkedin"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_facebook_url']) && $jomelle_options['footer_facebook_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_facebook_url'];?>" class="dnd_tooltip" title="<?php _e('Follow us on Facebook','ABdev_jomelle'); ?>" data-gravity="s" target="<?php echo $target; ?>"><i class="ci_icon-facebook"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_skype_url']) && $jomelle_options['footer_skype_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_skype_url'];?>" class="dnd_tooltip" title="<?php _e('Follow us on Skype','ABdev_jomelle'); ?>" data-gravity="s" target="<?php echo $target; ?>"><i class="ci_icon-skype"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_googleplus_url']) && $jomelle_options['footer_googleplus_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_googleplus_url'];?>" class="dnd_tooltip" title="<?php _e('Follow us on Googleplus','ABdev_jomelle'); ?>" data-gravity="s" target="<?php echo $target; ?>"><i class="ci_icon-googleplus"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_twitter_url']) && $jomelle_options['footer_twitter_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_twitter_url'];?>" class="dnd_tooltip" title="<?php _e('Follow us on Twitter','ABdev_jomelle'); ?>" data-gravity="s" target="<?php echo $target; ?>"><i class="ci_icon-twitter"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_feed_url']) && $jomelle_options['footer_feed_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_feed_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Feed','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-rss"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_behance_url']) && $jomelle_options['footer_behance_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_behance_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Behance','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-behance"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_blogger_url']) && $jomelle_options['footer_blogger_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_blogger_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Blogger','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-blogger-blog"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_delicious_url']) && $jomelle_options['footer_delicious_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_delicious_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Delicious','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-delicious"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_designContest_url']) && $jomelle_options['footer_designContest_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_designContest_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on DesignContest','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-designcontest"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_deviantART_url']) && $jomelle_options['footer_deviantART_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_deviantART_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on DeviantART','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-deviantart"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_digg_url']) && $jomelle_options['footer_digg_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_digg_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Digg','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-digg"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_dribbble_url']) && $jomelle_options['footer_dribbble_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_dribbble_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Dribbble','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-dribbble"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_dropbox_url']) && $jomelle_options['footer_dropbox_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_dropbox_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Dropbox','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-dropbox"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_email_url']) && $jomelle_options['footer_email_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_email_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Email','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-email"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_flickr_url']) && $jomelle_options['footer_flickr_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_flickr_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Flickr','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-flickr"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_forrst_url']) && $jomelle_options['footer_forrst_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_forrst_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Forrst','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-forrst"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_instagram_url']) && $jomelle_options['footer_instagram_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_instagram_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Instagram','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-instagram"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_last.fm_url']) && $jomelle_options['footer_last.fm_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_last.fm_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Last.fm','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-lastfm"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_myspace_url']) && $jomelle_options['footer_myspace_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_myspace_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on MySpace','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-myspace"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_picasa_url']) && $jomelle_options['footer_picasa_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_picasa_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Picasa','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-picasa"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_stumbleUpon_url']) && $jomelle_options['footer_stumbleUpon_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_stumbleUpon_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on StumbleUpon','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-stumbleupon"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_vimeo_url']) && $jomelle_options['footer_vimeo_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_vimeo_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Vimeo','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-vimeo"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_zerply_url']) && $jomelle_options['footer_zerply_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_zerply_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Zerply','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-zerply"></i></a>
						<?php endif;?>
						<?php if(isset($jomelle_options['footer_pinterest_url']) && $jomelle_options['footer_pinterest_url'] != ''):?>
							<a href="<?php echo $jomelle_options['footer_pinterest_url'];?>" class="top_social_icon dnd_tooltip" title="<?php _e('Follow us on Pinterest','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-pinterest"></i></a>
						<?php endif;?>

					</div>
				</div>
			</div>
		</div>
	</footer>

	<?php echo (isset($jomelle_options['analytics_code'])) ? $jomelle_options['analytics_code'] : ''; ?>

	<?php wp_footer(); ?>
	
</body>
</html>