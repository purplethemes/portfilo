<?php

/*********** Shortcode: Spacer ************************************************************/

$ABdevDND_shortcodes['spacer_dd'] = array(
	'attributes' => array(
		'pixels' => array(
			'default' => '15',
			'description' => __('Height in Pixels', 'dnd-shortcodes'),
		),
		'responsive_hide' => array(
			'description' => __( 'Hide on Responsive', 'dnd-shortcodes' ),
			'default' => '0',
			'type' => 'checkbox',
		),
	),
	'description' => __('Spacer', 'dnd-shortcodes'),
	'info' => __('This shortcode will add additional vertical space between elements', 'dnd-shortcodes')
);
function ABdevDND_spacer_dd_shortcode( $attributes ) {
    extract(shortcode_atts(ABdevDND_extract_attributes('spacer_dd'), $attributes));

    $responsive_hide = ($responsive_hide) ? 'spacer_responsive_hide' : '';

    return '<span class="clear '.$responsive_hide.'" style="height:'.$pixels.'px;display:block;"></span>';
}


