<?php
/*
Template Name: 404 page
*/

get_header();

get_template_part('partials/header_menu'); 
get_template_part('partials/title_breadcrumbs_bar');

?>

	<section id="page404" class="container">
		<p class="big_404"><?php _e('404', 'ABdev_jomelle') ?></p>
		<h2><?php _e('Page Not Found', 'ABdev_jomelle') ?></h2>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
			<?php the_content();?>
		<?php endwhile; endif;?>
	</section>

<?php get_footer();