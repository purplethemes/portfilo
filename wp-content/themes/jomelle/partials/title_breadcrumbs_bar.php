<?php 
global $jomelle_options, $ABdev_title_bar_title;
?>

<?php if(!isset($jomelle_options['hide_title_breadcrumbs_bar']) || $jomelle_options['hide_title_breadcrumbs_bar']!=1): ?>
	<section id="title_breadcrumbs_bar">
		<div class="container">
			<div class="row">
				<div class="span8">
					<?php if(!isset($jomelle_options['hide_title_from_bar']) || $jomelle_options['hide_title_from_bar']!=1): ?>
						<h1><?php echo (!empty($ABdev_title_bar_title)) ? $ABdev_title_bar_title : get_the_title();?></h1>
					<?php endif; ?>
				</div>
				<div class="span4 right_aligned">
					<?php if(!isset($jomelle_options['hide_breadcrumbs_from_bar']) || $jomelle_options['hide_breadcrumbs_from_bar']!=1): ?>
						<?php ABdevFW_simple_breadcrumb(); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>