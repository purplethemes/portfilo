<?php 
	global $jomelle_options;
?>

<div id="top_bar">
	<div class="container">
		<div class="row">
			<div class="span6 dnd_follow_us">
				<?php 
				$target = (isset($jomelle_options['header_social_target'])) ? $jomelle_options['header_social_target'] : '_blank';
				?>

				<?php if(isset($jomelle_options['header_facebook_url']) && $jomelle_options['header_facebook_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_facebook_url'];?>" class="top_social_icon dnd_socialicon_facebook dnd_tooltip" title="<?php _e('Follow us on Facebook','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-facebook"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_twitter_url']) && $jomelle_options['header_twitter_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_twitter_url'];?>" class="top_social_icon dnd_socialicon_twitter dnd_tooltip" title="<?php _e('Follow us on Twitter','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-twitter"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_googleplus_url']) && $jomelle_options['header_googleplus_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_googleplus_url'];?>" class="top_social_icon dnd_socialicon_googleplus dnd_tooltip" title="<?php _e('Follow us on Googleplus','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-googleplus"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_linkedin_url']) && $jomelle_options['header_linkedin_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_linkedin_url'];?>" class="top_social_icon dnd_socialicon_linkedin dnd_tooltip" title="<?php _e('Follow us on Linkedin','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-linkedin"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_youtube_url']) && $jomelle_options['header_youtube_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_youtube_url'];?>" class="top_social_icon dnd_socialicon_youtube dnd_tooltip" title="<?php _e('Follow us on Youtube','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-youtube"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_pinterest_url']) && $jomelle_options['header_pinterest_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_pinterest_url'];?>" class="top_social_icon dnd_socialicon_pinterest dnd_tooltip" title="<?php _e('Follow us on Pinterest','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-pinterest"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_skype_url']) && $jomelle_options['header_skype_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_skype_url'];?>" class="top_social_icon dnd_socialicon_skype dnd_tooltip" title="<?php _e('Follow us on Skype','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-skype"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_feed_url']) && $jomelle_options['header_feed_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_feed_url'];?>" class="top_social_icon dnd_socialicon_feed dnd_tooltip" title="<?php _e('Follow us on Feed','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-rss"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_behance_url']) && $jomelle_options['header_behance_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_behance_url'];?>" class="top_social_icon dnd_socialicon_behance dnd_tooltip" title="<?php _e('Follow us on Behance','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-behance"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_blogger_url']) && $jomelle_options['header_blogger_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_blogger_url'];?>" class="top_social_icon dnd_socialicon_blogger dnd_tooltip" title="<?php _e('Follow us on Blogger','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-blogger-blog"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_delicious_url']) && $jomelle_options['header_delicious_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_delicious_url'];?>" class="top_social_icon dnd_socialicon_delicious dnd_tooltip" title="<?php _e('Follow us on Delicious','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-delicious"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_designContest_url']) && $jomelle_options['header_designContest_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_designContest_url'];?>" class="top_social_icon dnd_socialicon_designContest dnd_tooltip" title="<?php _e('Follow us on DesignContest','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-designcontest"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_deviantART_url']) && $jomelle_options['header_deviantART_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_deviantART_url'];?>" class="top_social_icon dnd_socialicon_devianART dnd_tooltip" title="<?php _e('Follow us on DeviantART','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-deviantart"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_digg_url']) && $jomelle_options['header_digg_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_digg_url'];?>" class="top_social_icon dnd_socialicon_digg dnd_tooltip" title="<?php _e('Follow us on Digg','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-digg"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_dribbble_url']) && $jomelle_options['header_dribbble_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_dribbble_url'];?>" class="top_social_icon dnd_socialicon_dribbble dnd_tooltip" title="<?php _e('Follow us on Dribbble','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-dribbble"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_dropbox_url']) && $jomelle_options['header_dropbox_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_dropbox_url'];?>" class="top_social_icon dnd_socialicon_dropbox dnd_tooltip" title="<?php _e('Follow us on Dropbox','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-dropbox"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_email_url']) && $jomelle_options['header_email_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_email_url'];?>" class="top_social_icon dnd_socialicon_email dnd_tooltip" title="<?php _e('Follow us on Email','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-email"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_flickr_url']) && $jomelle_options['header_flickr_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_flickr_url'];?>" class="top_social_icon dnd_socialicon_flickr dnd_tooltip" title="<?php _e('Follow us on Flickr','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-flickr"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_forrst_url']) && $jomelle_options['header_forrst_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_forrst_url'];?>" class="top_social_icon dnd_socialicon_forrst dnd_tooltip" title="<?php _e('Follow us on Forrst','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-forrst"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_instagram_url']) && $jomelle_options['header_instagram_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_instagram_url'];?>" class="top_social_icon dnd_socialicon_instagram dnd_tooltip" title="<?php _e('Follow us on Instagram','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-instagram"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_last.fm_url']) && $jomelle_options['header_last.fm_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_last.fm_url'];?>" class="top_social_icon dnd_socialicon_last.fm dnd_tooltip" title="<?php _e('Follow us on Last.fm','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-lastfm"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_myspace_url']) && $jomelle_options['header_myspace_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_myspace_url'];?>" class="top_social_icon dnd_socialicon_myspace dnd_tooltip" title="<?php _e('Follow us on MySpace','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-myspace"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_picasa_url']) && $jomelle_options['header_picasa_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_picasa_url'];?>" class="top_social_icon dnd_socialicon_picasa dnd_tooltip" title="<?php _e('Follow us on Picasa','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-picasa"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_stumbleUpon_url']) && $jomelle_options['header_stumbleUpon_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_stumbleUpon_url'];?>" class="top_social_icon dnd_socialicon_stumbleUpon dnd_tooltip" title="<?php _e('Follow us on StumbleUpon','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-stumbleupon"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_vimeo_url']) && $jomelle_options['header_vimeo_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_vimeo_url'];?>" class="top_social_icon dnd_socialicon_vimeo dnd_tooltip" title="<?php _e('Follow us on Vimeo','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-vimeo"></i></a>
				<?php endif;?>
				<?php if(isset($jomelle_options['header_zerply_url']) && $jomelle_options['header_zerply_url'] != ''):?>
					<a href="<?php echo $jomelle_options['header_zerply_url'];?>" class="top_social_icon dnd_socialicon_zerply dnd_tooltip" title="<?php _e('Follow us on Zerply','ABdev_jomelle'); ?>" data-gravity="n" target="<?php echo $target; ?>"><i class="ci_icon-zerply"></i></a>
				<?php endif;?>

			</div>

			<div id="header_phone_email_info" class="span6 right_aligned">
				<?php 
				echo (isset($jomelle_options['header_phone']) && $jomelle_options['header_phone'] != '') ? '<span><i class="ci_icon-phonealt"></i>'.$jomelle_options['header_phone'].'</span>' : '';
				echo (isset($jomelle_options['header_email']) && $jomelle_options['header_email'] != '') ? '<span><i class="ci_icon-emailalt"></i><a href="mailto:'.$jomelle_options['header_email'].'">'.$jomelle_options['header_email'].'</a></span>' : '';
				?>
			</div>
			
		</div>
	</div>
</div>

<header id="Abdev_main_header" class="clearfix">
	<div class="container">
		<div id="logo">
			<a href="<?php echo home_url(); ?>"><img src="<?php echo (isset($jomelle_options['header_logo']['url']) && $jomelle_options['header_logo']['url'] != '') ? $jomelle_options['header_logo']['url'] : TEMPPATH.'/images/logo.png';?>" alt="<?php bloginfo('name');?>"></a>
		</div>
		<div class="search-toggle">
			<a class="search-icon"><i class="ci_icon-search"></i></a>
			<div id="search-container" class="search-box-wrapper hide">
				<div class="search-box">
				    <?php get_template_part('partials/header_search_default'); ?>
				</div>
			</div>
		</div>
		<nav>
			<?php wp_nav_menu( array( 'theme_location' => 'header-menu','container' => false,'menu_id' => 'main_menu','menu_class' => '','walker'=> new jomelle_walker_nav_menu, 'fallback_cb' => false ) );?>
		</nav>
		<div id="ABdev_menu_toggle"><i class="ci_icon-menu"></i></div>
	</div>
</header>
