<?php 

if ( ! function_exists( 'ABdev_colors_css_hex2rgb' ) ){
	function ABdev_colors_css_hex2rgb($hex) {
		$hex = str_replace("#", "", $hex);
		if(strlen($hex) == 3) {
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} else {
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		$rgb = array($r, $g, $b);
		return implode(",", $rgb); 
	} 
}

if ( ! function_exists( 'ABdev_colors_css_adjustBrightness' ) ){
	function ABdev_colors_css_adjustBrightness($hex, $steps) {
		// Steps should be between -255 and 255. Negative = darker, positive = lighter
		$steps = max(-255, min(255, $steps));
		$hex = str_replace('#', '', $hex);
		if (strlen($hex) == 3) {
			$hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
		}
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
		$r = max(0,min(255,$r + $steps));
		$g = max(0,min(255,$g + $steps));  
		$b = max(0,min(255,$b + $steps));
		$r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
		$g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
		$b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);
		return '#'.$r_hex.$g_hex.$b_hex;
	}
}



if(isset($jomelle_options['main_color']) && $jomelle_options['main_color'] != ''){ 
	$main_color = $jomelle_options['main_color'];
	$hover_shadow = ABdev_colors_css_adjustBrightness($main_color, '-20');
	$hover_rgba = 'rgba('.ABdev_colors_css_hex2rgb($main_color).',0.9)';
	$custom_css.= '
.dnd_section_dd header h3:after{background: '.$main_color.';}
.column_title_left:after{background: '.$main_color.';}
.dnd-tabs .ui-tabs-nav li:hover a{color: #fff;background: '.$main_color.';}
.dnd-tabs .ui-tabs-nav li.ui-tabs-active {border-top: 2px solid '.$main_color.';}
.dnd-tabs .ui-tabs-nav li.ui-tabs-active a{color: '.$main_color.';}
.dnd-tabs.dnd-tabs-timeline ul li:hover a{color: '.$main_color.';}
.dnd-tabs .ui-tabs-nav li.ui-tabs-active:hover a{color: '.$main_color.';}
.dnd-tabs.dnd-tabs-timeline .ui-tabs-nav li.ui-tabs-active:before{border: 3px solid '.$main_color.';background: #fff;}
.dnd-accordion .ui-accordion-header-active { color:'.$main_color.';}
.dnd-accordion .ui-accordion-header:hover { color:#ffffff;background-color:  '.$main_color.';}
.dnd-accordion .ui-icon-triangle-1-s{background: '.$main_color.';}
.dnd-table.dnd-table-alternative th{color: #fff;background: '.$main_color.';}
.dnd_blockquote_style1{color:  #73767e;background-color:  #f7f9fa;border-left: 4px solid '.$main_color.';}
.dnd_stats_excerpt{color: '.$main_color.';}
.dnd_team_member .dnd_team_member_link:hover span{color: '.$main_color.';}
.dnd_team_member_modal .dnd_meter .dnd_meter_percentage{background: '.$main_color.' !important;}
.dnd_latest_news_time{color: #fff;background: '.$main_color.';}
.dnd_latest_news_shortcode_content h5 a:hover{color: '.$main_color.';}
.dnd_pricing-table-2 .dnd_popular-plan .dnd_pricebox_name{color: #fff;background: '.$main_color.';}
.dnd_service_box h3:hover{color: '.$main_color.';}
.dnd_service_box .dnd_icon_boxed i{color: '.$main_color.';}
.dnd_service_box:hover .dnd_icon_boxed{background: '.$main_color.';}
.dnd_service_box .dnd_icon_boxed:hover:after{border-top: 9px solid '.$main_color.';}
.dnd_service_box.dnd_service_box_round_stroke .dnd_icon_boxed{border: 2px solid '.$main_color.';}
.dnd_service_box.dnd_service_box_round_stroke .dnd_icon_boxed i{color: '.$main_color.';}
.dnd_service_box.dnd_service_box_round_stroke:hover .dnd_icon_boxed{background: '.$main_color.';border: 2px solid '.$main_color.';}
.dnd_service_box.dnd_service_box_round_aside h3:hover{color: '.$main_color.';}
.dnd_service_box.dnd_service_box_round_aside_white h3:hover{color: '.$main_color.';}
.dnd_service_box.dnd_service_box_round_aside_white .dnd_icon_boxed{border: 2px solid #fff;}
.dnd_service_box.dnd_service_box_round_aside_white:hover .dnd_icon_boxed{background-color: '.$main_color.';}
.dnd_dropcap{background: '.$main_color.';}
.dnd_dropcap.dnd_dropcap_style2{background: '.$main_color.';color: #fff;}
.dnd-button_orange{background: '.$main_color.';border: 1px solid '.$main_color.';color: #fff !important;}
.process_section .dnd_icon_boxed i{color: '.$main_color.';}
.process_section .dnd_icon_boxed:hover{background: '.$main_color.';}
.process_section .dnd_service_box h3:hover{color: '.$main_color.';}
.section_with_alternative_header header {background-color:  '.$main_color.';}
.section_with_alternative_header header:after {border-top: 21px solid '.$main_color.';}
.column_title_text_margin:after,.column_title_margin:after{background: '.$main_color.';}
a{color: '.$main_color.';}
.orange_title{color:  '.$main_color.';}
button,input[type="submit"] {border: 1px solid '.$main_color.';background: '.$main_color.';color: #fff;}
.color_highlight{color: '.$main_color.';}
.section_color_background{background: '.$main_color.';}
.leading_line:after{background: '.$main_color.';}
#header_phone_email_info span a:hover{color: '.$main_color.';}
nav > ul > li a:hover{color: '.$main_color.';}
nav > ul ul{background: #fff;border-top: 2px solid '.$main_color.';}
nav > ul > .menu-item-has-children:hover > a:after{border-bottom: 6px solid '.$main_color.';}
nav > ul ul li:hover > a{color: '.$main_color.';}
nav > ul > .current-menu-item > a,nav > ul > .current-post-ancestor > a,nav > ul > .current-menu-ancestor > a{color: '.$main_color.';}
.sf-mega-inner{background: #191b1f;border-top: 2px solid '.$main_color.';}
nav > ul .sf-mega-inner > ul > li > ul > li:hover a{color: '.$main_color.';}
nav > ul .sf-mega-inner .description_menu_item a{color: '.$main_color.';}
.search-toggle:hover .search-icon i:first-of-type{    color: #fff;    background: '.$main_color.';}
#search-container .widget_search i{color: '.$main_color.';}
.search-icon.active i{    background: '.$main_color.';    color: #fff;}
.search-toggle.active{background: '.$main_color.';color: #fff;}
.tp-leftarrow.default:hover,.tp-rightarrow.default:hover {background: '.$main_color.' !important;}
.jomelle_button_white2:hover{background: '.$main_color.' !important;}
.jomelle_button_white1:hover{background: '.$main_color.' !important;}
.jomelle_button{background: '.$main_color.' !important;}
.jomelle_button:hover{border: 1px solid '.$main_color.';background: #fff !important;}
.jomelle_button:hover a{color: '.$main_color.';}
.jomelle_big_orange {color: '.$main_color.';}
.jomelle_orange_text{color: '.$main_color.';}
.timeline_post h4 a:hover{color: '.$main_color.';}
.timeline_postmeta a:hover{color: '.$main_color.';}
.timeline_post .post-readmore .more-link{color: '.$main_color.';}
.timeline_post_left:after,.timeline_post_right:after{background: '.$main_color.';}
.post_content .post_main h2 a:hover{color:  '.$main_color.';}
.post_content .post_main .post_meta a:hover{color: '.$main_color.';}
.post_main .post-readmore .more-link{color:  '.$main_color.';}
.post_meta_tags a:hover{color: '.$main_color.';}
.post_main .postmeta-under a:hover{color: '.$main_color.';}
.comment .reply,.comment .edit-link,.comment .reply a,.comment .edit-link a{color: '.$main_color.';}
#respond #comment-submit{background: '.$main_color.';}
#blog_pagination .page-numbers:hover{background: '.$main_color.';color: #fff;border: 1px solid '.$main_color.';}
#blog_pagination .page-numbers.current{color: '.$main_color.';border: 1px solid '.$main_color.';}
.wpcf7-submit{background: '.$main_color.';color: #fff;}
#Abdev_contact_form_submit{background: '.$main_color.' !important;}
.widget a:hover{color: '.$main_color.';}
.widget_search i{color: '.$main_color.';}
.tagcloud a:hover{background: '.$main_color.';color: #fff !important;border: 1px solid '.$main_color.';}
.portfolio_item h4 a:hover{color: '.$main_color.';}
.portfolio_item_view_link a{color: '.$main_color.';}
.portfolio_item:hover .overlayed .overlay{background: '.$hover_rgba.';}
#filters li a.selected,#filters li a:hover{color: #fff;background-color:  '.$main_color.';}
#page404 .big_404{color: '.$main_color.';}
#page404 .dnd_search i{color:  '.$main_color.';}
.ABt_testimonials_wrapper .ABt_pagination .selected{background-color:  '.$main_color.';border: 2px solid '.$main_color.';}
.ABt_testimonials_wrapper .ABt_pagination a:hover{background-color:  '.$main_color.';border: 2px solid '.$main_color.';}
.ABt_testimonials_wrapper.picture_top .ABt_pagination a.selected{background: '.$main_color.';}
.ABt_testimonials_wrapper.picture_top .ABt_pagination a:hover{background: '.$main_color.';}
.ab-tweet-scroller:before{color: '.$main_color.';}
.ab-tweet-username{color: '.$main_color.';}
.ab-tweet-text a{color: '.$main_color.';}
.ab-tweet-date{color: '.$main_color.';}
.ab-tweet-prev:after{color: '.$main_color.';}
.ab-tweet-next:after{color: '.$main_color.';}

		';
}


