<?php 

// ***************** 3rd party shortcode support ***************************************


// *************** ab-testimonials plugin support ********************

if( in_array('ab-testimonials/ab-testimonials.php', get_option('active_plugins')) ){
	$ABdev_shortcodes['AB_testimonials'] = array(
		'name' => 'AB_testimonials',
		'type' => 'single',
		'third_party' => 1, 
		'atts' => array(
			'category' => array(
				'desc' => __( 'Testimonial Category', 'ABdev_jomelle' ),
			),
			'count' => array(
				'desc' => __( 'Number of Testimonials to Show', 'ABdev_jomelle' ),
				'default' => '8',
			),
			'show_arrows' => array(
				'default' => '0',
				'type' => 'checkbox',
				'desc' => __( 'Show Navigation Arrows', 'ABdev_jomelle' ),
			),
			'timeoutduration' => array(
				'desc' => __( 'Delay', 'ABdev_jomelle' ),
				'default' => '5000',
			),
			'duration' => array(
				'desc' => __( 'Animation Duration', 'ABdev_jomelle' ),
				'default' => '1000',
			),
			'style' => array(
				'desc' => __( 'Style', 'ABdev_jomelle' ),
				'default' => '1',
				'values' => array(
					'1' =>  __( 'Big without image', 'ABdev_jomelle' ),
					'2' => __( 'Small with image', 'ABdev_jomelle' ),
				),
			),
			'fx' => array(
				'desc' => __( 'Transition Effect', 'ABdev_jomelle' ),
				'default' => 'crossfade',
				'values' => array(
					'none' =>  __( 'None', 'ABdev_jomelle' ),
					'fade' =>  __( 'Fade', 'ABdev_jomelle' ),
					'crossfade' =>  __( 'Crossfade', 'ABdev_jomelle' ),
					'cover-fade' =>  __( 'Cover Fade', 'ABdev_jomelle' ),
				),
			),
			'easing' => array(
				'desc' => __( 'Easing Effect', 'ABdev_jomelle' ),
				'default' => 'quadratic',
				'values' => array(
					'linear' =>  __( 'Linear', 'ABdev_jomelle' ),
					'swing' =>  __( 'Swing', 'ABdev_jomelle' ),
					'quadratic' =>  __( 'Quadratic', 'ABdev_jomelle' ),
					'cubic' =>  __( 'Cubic', 'ABdev_jomelle' ),
					'elastic' =>  __( 'Elastic', 'ABdev_jomelle' ),
				),
			),
			'direction' => array(
				'desc' => __( 'Slide Direction', 'ABdev_jomelle' ),
				'default' => 'left',
				'values' => array(
					'left' =>  __( 'Left', 'ABdev_jomelle' ),
					'right' =>  __( 'Right', 'ABdev_jomelle' ),
				),
			),
			'play' => array(
				'default' => '0',
				'type' => 'checkbox',
				'desc' => __( 'Autoplay', 'ABdev_jomelle' ),
			),
			'pauseOnHover' => array(
				'desc' => __( 'Pause on Hover', 'ABdev_jomelle' ),
				'default' => 'immediate',
				'values' => array(
					'false' =>  __( 'No', 'ABdev_jomelle' ),
					'resume' =>  __( 'Resume', 'ABdev_jomelle' ),
					'immediate' =>  __( 'Immediate', 'ABdev_jomelle' ),
				),
			),
			'class' => array(
				'default' => '',
				'desc' => __( 'Class', 'ABdev_jomelle' ),
			),
		),
		'desc' => __( 'AB Testimonial Slider', 'ABdev_jomelle' )
	);
}