<?php 

get_header();

get_template_part('partials/header_menu'); 


$cat_id = get_query_var('cat');
$cat_data = get_option("category_$cat_id"); 

global $ABdev_title_bar_title;

$ABdev_title_bar_title  = __('Blog','ABdev_jomelle');

if(is_category()){
	$thisCat = get_category(get_query_var('cat'), false);
	$ABdev_title_bar_title = $thisCat -> name;
}
elseif(is_author()){
	$curauth = get_userdata(get_query_var('author'));
	$ABdev_title_bar_title = __('Posts by','ABdev_jomelle') . ' ' . $curauth -> display_name;
}
elseif(is_tag()){
	$ABdev_title_bar_title = __('Posts Taged','ABdev_jomelle').' '.get_query_var('tag');
}
elseif(is_month()){
	$month = '01-'.substr(get_query_var('m'), 4, 2).'-'.substr(get_query_var('m'), 0, 4);
	$ABdev_title_bar_title = __('Posts on ','ABdev_jomelle').' '.date('M Y',strtotime($month));
}

get_template_part('partials/title_breadcrumbs_bar'); 

?>
	
	<section>
		<div class="container">
			
			<?php if($cat_data['sidebar_position']=='timeline'): 
				$i = 0;
			?>
				<div id="timeline_posts" class="clearfix">
				<?php if (have_posts()) :  while (have_posts()) : the_post(); 
					$i++;
					$classes = array();
					$classes[] = 'timeline_post';
					if($i==1){
						$classes[] = 'timeline_post_first';
					}
				?>
					<div <?php post_class($classes); ?>>
						
						<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						
						<?php
						$custom = get_post_custom();

						if(isset($custom['ABdevFW_selected_media'][0]) && $custom['ABdevFW_selected_media'][0]=='soundcloud' && isset($custom['ABdevFW_soundcloud'][0]) && $custom['ABdevFW_soundcloud'][0]!=''){
							echo '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F'.$custom['ABdevFW_soundcloud'][0].'"></iframe>';
						}
						elseif(isset($custom['ABdevFW_selected_media'][0]) && $custom['ABdevFW_selected_media'][0]=='youtube' && isset($custom['ABdevFW_youtube_id'][0]) && $custom['ABdevFW_youtube_id'][0]!=''){
							echo '<div class="videoWrapper-youtube"><iframe src="http://www.youtube.com/embed/'.$custom['ABdevFW_youtube_id'][0].'?showinfo=0&amp;autohide=1&amp;related=0" frameborder="0" allowfullscreen></iframe></div>';
						}
						elseif(isset($custom['ABdevFW_selected_media'][0]) && $custom['ABdevFW_selected_media'][0]=='vimeo' && isset($custom['ABdevFW_vimeo_id'][0]) && $custom['ABdevFW_vimeo_id'][0]!=''){
							echo '<div class="videoWrapper-vimeo"><iframe src="http://player.vimeo.com/video/'.$custom['ABdevFW_vimeo_id'][0].'?title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
						}
						else{
							the_post_thumbnail();
						}
						?>
						
						<div class="timeline_postmeta">
							<p class="post_meta_date"><i class="ci_icon-time"></i><?php the_date('M j, Y'); ?></p>
							<p class="post_meta_author"><i class="ci_icon-user"></i><?php the_author_posts_link(); ?></p>
							<p class="post_meta_comments"><i class="ci_icon-comment"></i><?php echo get_comments_number(); ?></p>
						</div>
						
						<div class="timeline_content">
							<?php the_content('');?>
						</div>
						
						<div class="post-readmore">
							<p class="post_meta_tags"><?php the_tags( '<i class="ci_icon-tags"></i>',', ', ''); ?></p>
							<a href="<?php the_permalink(); ?>" class="more-link"><?php _e('Read More', 'ABdev_jomelle'); ?><i class="ci_icon-chevron-right"></i></a>
						</div>

					</div>
				<?php endwhile; 
				else: ?>
					<p><?php _e('No posts were found. Sorry!', 'ABdev_jomelle'); ?></p>
				<?php endif; ?>
				</div>
				<div id="timeline_loading" data-ajaxurl="<?php echo TEMPPATH; ?>/php/timeline-ajax.php?cat=<?php echo $cat_id; ?>" data-noposts="<?php _e('No older posts found', 'ABdev_jomelle'); ?>"></div>


			<?php else: ?>
				<div class="row">

					<div class="blog_category_index <?php echo (isset($cat_data['sidebar_position']) && $cat_data['sidebar_position']=='none')?'span12':'span8';?> <?php echo (isset($cat_data['sidebar_position']) && $cat_data['sidebar_position']=='left')?'content_with_left_sidebar':'content_with_right_sidebar';?>">
						<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
							<?php $custom = get_post_custom(); ?>
								<div <?php post_class('post_wrapper clearfix'); ?>>
									<div class="post_content">
										<div class="post_main">
											<?php
											if(isset($custom['ABdevFW_selected_media'][0]) && $custom['ABdevFW_selected_media'][0]=='soundcloud' && isset($custom['ABdevFW_soundcloud'][0]) && $custom['ABdevFW_soundcloud'][0]!=''){
												echo '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F'.$custom['ABdevFW_soundcloud'][0].'"></iframe>';
											}
											elseif(isset($custom['ABdevFW_selected_media'][0]) && $custom['ABdevFW_selected_media'][0]=='youtube' && isset($custom['ABdevFW_youtube_id'][0]) && $custom['ABdevFW_youtube_id'][0]!=''){
												echo '<div class="videoWrapper-youtube"><iframe src="http://www.youtube.com/embed/'.$custom['ABdevFW_youtube_id'][0].'?showinfo=0&amp;autohide=1&amp;related=0" frameborder="0" allowfullscreen></iframe></div>';
											}
											elseif(isset($custom['ABdevFW_selected_media'][0]) && $custom['ABdevFW_selected_media'][0]=='vimeo' && isset($custom['ABdevFW_vimeo_id'][0]) && $custom['ABdevFW_vimeo_id'][0]!=''){
												echo '<div class="videoWrapper-vimeo"><iframe src="http://player.vimeo.com/video/'.$custom['ABdevFW_vimeo_id'][0].'?title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
											}
											else{
												the_post_thumbnail();
											}
											?>
											<h2><a href="<?php the_permalink(); ?>"><?php echo ($post->post_excerpt!=='') ? get_the_excerpt() : get_the_title(); ?></a></h2>
											<div class="post_meta">
												<span class="post_author"><i class="ci_icon-user"></i><?php the_author(); ?></span> 
												<span class="post_date"><i class="ci_icon-time"></i><?php the_date('M j, Y'); ?></span> 
												<span class="post_tags"><i class="ci_icon-tags"></i><?php the_tags( '',', ', ''); ?></span>
												<span class="post_comments"><i class="ci_icon-chat"></i><?php comments_number('','',''); ?></span>
											</div>
											<?php the_content('');?>
											<div class="post-readmore">
												<a href="<?php the_permalink(); ?>" class="more-link"><?php _e('Continue Reading', 'ABdev_jomelle'); ?><i class="ci_icon-chevron-right"></i></a>
											</div>
										</div>
									</div>
								</div>
								
							
						<?php endwhile; 
						else: ?>
							<p><?php _e('No posts were found. Sorry!', 'ABdev_jomelle'); ?></p>
						<?php endif; ?>
						
						
					</div><!-- end span8 main-content -->
					
					<?php if (!isset($cat_data['sidebar_position']) || (isset($cat_data['sidebar_position']) && $cat_data['sidebar_position'] != 'none')):?>
						<aside class="span4 sidebar <?php echo (isset($cat_data['sidebar_position']) && $cat_data['sidebar_position']=='left')?'sidebar_left':'sidebar_right';?>">
							<?php 
							if(isset($cat_data['sidebar']) && $cat_data['sidebar']!=''){
								$selected_sidebar=$cat_data['sidebar'];
							}
							else{
								$selected_sidebar=__( 'Primary Sidebar', 'ABdev_jomelle');
							}
							dynamic_sidebar($selected_sidebar);
							?>
						</aside><!-- end span4 sidebar -->
					<?php endif; ?>

				</div><!-- end row -->

			<?php endif; ?>
		<?php 
		if($cat_data['sidebar_position']!='timeline'){
			get_template_part( 'partials/pagination' );
		}
		?>
		</div>
	</section>


<?php get_footer();