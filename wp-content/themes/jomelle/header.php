<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php
	global $jomelle_options;
?>
<title><?php bloginfo('name'); wp_title(' - ',true); ?></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="<?php bloginfo('description'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo (isset($jomelle_options['favicon']['url']) && $jomelle_options['favicon']['url'] != '') ? $jomelle_options['favicon']['url'] : TEMPPATH.'/images/favicon.png';?>" />
		
<!--[if lt IE 9]>
  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php 
$classes=array();

if(isset($jomelle_options['enable_preloader']) && $jomelle_options['enable_preloader']==1){
	 $classes[] = 'preloader';
}

if(isset($jomelle_options['fixed_header']) && $jomelle_options['fixed_header']=='fixed'){
	$classes[] = 'fixed_header';
}

if(isset($jomelle_options['main_layout']) && $jomelle_options['main_layout']=='boxed'){
	$classes[] = 'boxed_layout';
}

$classes = implode(' ', $classes);

if ( is_singular() ){
	wp_enqueue_script( "comment-reply" );
}
wp_head();
?>
</head>

<body <?php body_class($classes); ?>>




