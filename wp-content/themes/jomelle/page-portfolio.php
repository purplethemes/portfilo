<?php

/*
Template Name: Portfolio - Single Column
*/

$read_more=__('Read More','ABdev_jomelle');

get_header();

get_template_part('partials/header_menu'); 
get_template_part('partials/title_breadcrumbs_bar'); 

?>

<?php //check if portfolio plugin is activated
if(current_user_can( 'manage_options' ) && !in_array( 'abdev-portfolio/abdev-portfolio.php', get_option( 'active_plugins') )):?>
	<section>
		<div class="container">
			<p>
				<strong><?php _e('This page requires Portfolio plugin to be activated','ABdev_jomelle');?></strong>
			</p>
		</div>
	</section>
<?php 
endif; 

if (have_posts()) : while (have_posts()) : the_post();
	$content = do_shortcode(get_the_content());
	if ($content != ''):?>
		<section>
			<div class="container">
				<?php echo $content;?>
			</div>
		</section>
<?php endif; endwhile; endif;?>



<section class="<?php echo ($content != '') ? 'section_border_top' : '';?>">
	<div class="container">
		<div id="portfolio_single_column">
			<?php
			$values = get_post_custom( $post->ID );
			$selected_categories = isset($values['categories'][0]) ? $values['categories'][0] : '';
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			$args = array(
				'post_type' => 'portfolio',
				'portfolio-category' => $selected_categories,
				'paged'=>$paged,
			);
			$posts = new WP_Query( $args );
			$out = $error = '';
			if ($posts->have_posts()){
				while ($posts->have_posts()){
					$posts->the_post();
					$portfolio_data = get_post_custom();
					?>
					<div class="portfolio_single_column_item">
						<div class="row">
							<div class="span6">
								<?php echo get_the_post_thumbnail();?>
							</div>
							<div class="span6">

								<div id="portfolio_item_meta">
									<h2 class="column_title_left"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>

									<?php the_content();?>
									
									<h5 class=""><?php _e('Details', 'ABdev_jomelle') ?></h5>

									<p class="portfolio_single_detail">
										<span class="portfolio_item_meta_label"><?php _e('Date:', 'ABdev_jomelle');?></span>
										<span class="portfolio_item_meta_data"><?php the_date();?></span>
									</p>

									<?php if(isset($portfolio_data['ABp_portfolio_client'][0])):?>
										<p class="portfolio_single_detail">
											<span class="portfolio_item_meta_label"><?php _e('Client:', 'ABdev_jomelle');?></span>
											<span class="portfolio_item_meta_data"><?php echo $portfolio_data['ABp_portfolio_client'][0];?></span>
										</p>
									<?php endif; ?>

									<p class="portfolio_single_detail">
										<span class="portfolio_item_meta_label"><?php _e('Category:', 'ABdev_jomelle');?></span>
										<span class="portfolio_item_meta_data">
											<?php 
											$categories=$related_cat='';
											$terms = get_the_terms( null , 'portfolio-category' );
											
											if(is_array($terms)){
												foreach ( $terms as $term ) {
													if(is_object($term)){
														$categories[] = $term->name;
														$related_cat[] = $term->slug;
													}
												} 
											}
											$categories = is_array($categories) ? implode(', ', $categories) : '';
											echo $categories;
											?>
										</span>
									</p>

									<?php if(isset($portfolio_data['ABp_portfolio_skills'][0])):?>
										<p class="portfolio_single_detail">
											<span class="portfolio_item_meta_label"><?php _e('Skills used:', 'ABdev_jomelle');?></span>
											<span class="portfolio_item_meta_data"><?php echo $portfolio_data['ABp_portfolio_skills'][0];?></span>
										</p>
									<?php endif; ?>

									<div class="post-readmore portfolio-readmore">
										<a href="<?php the_permalink();?>" class="more-link"><?php _e('Read More', 'ABdev_jomelle');?>Read More<i class="ci_icon-chevron-right"></i></a>
									</div>

								</div>
							</div>
						</div>
					</div>
					<?php 
				}
			}
			else{
				echo '<p>' . __('No Portfolio Posts Found.', 'ABdev_jomelle') . '</p>';
			}
			?> 
		</div>
		<?php get_template_part( 'partials/pagination-portfolio' ); ?>
	</div>
</section>

<?php get_footer();