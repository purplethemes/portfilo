<?php

/**
 * Template Name: Contact Form
 * Description: A Page Template that display Contact Form.
 *
 * @package Portfolio
 * @author Purplethemes
 */
 
get_header();

global $portfolio_options; 


$marker_img_url = $portfolio_options['marker_image'] ['thumbnail'];
$marker_latitude = $portfolio_options['latitude'];
$marker_longitude = $portfolio_options['longitude'];
$marker_text = $portfolio_options['marker_detail'];

$post_id = $wp_query->get_queried_object_id(); 
$args=array(
  'page_id' => $post_id,
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 1,
  'caller_get_posts'=> 1
);
$myposts = get_posts( $args );

if ($portfolio_options['location_checkbox'] == 0 && $portfolio_options['contact_checkbox'] == 0 && $portfolio_options['ofc_hrs_checkbox'] == 0 )
	{
		$right_class = '';
		$left_class = 'col-xs-12 col-sm-12 col-md-12';
	}
	
else
	{
		
		$right_class = 'col-xs-12 col-sm-4 col-md-4';
		$left_class = 'col-xs-12 col-sm-8 col-md-8';
	}

foreach ( $myposts as $post ) : setup_postdata( $post );

?>

<div class="container">
    <section class="contact-section">
        <section class="row">
            <article class="<?php echo $left_class; ?>">
            <!-- contact form section -->
                <div class="contact-form wow fadeInLeft" data-wow-delay="0.2s">
                    <h3><?php the_title(); ?></h3>
                    <div class="title-divider"></div>
                    <?php the_content(); ?>
                    <?php// echo do_shortcode("[ai_contact_form]"); ?>
                </div>
            <!-- contact form section end -->
            </article>
            
            <article class="<?php echo $right_class; ?>">
            	<div class="contact-info">
                	<?php if($portfolio_options['location_checkbox'] == 1)
		                	{ ?>
								<div class="office-location wow fadeInRight" data-wow-delay="0.3s">
		                        	<h3><?php echo $portfolio_options['location_title']? $portfolio_options['location_title']:'Address'  ?></h3>
		                        	<div class="title-divider"></div>
		                        	
				              		<p>
					              		<?php 
					              		
						              		for($i=1; $i<=3; $i++)
						              		{
												if($portfolio_options['location_line_'.$i]) echo $portfolio_options['location_line_'.$i].'<br/>'; 	
											}
					              	   ?>
					               	</p>
		              				
		                    	</div>
							<?php }
					
		                   	if($portfolio_options['contact_checkbox'] == 1)
		                    { ?>
								<div class="office-info wow fadeInRight" data-wow-delay="0.6s">
			                        <h3><?php echo $portfolio_options['contact_title']? $portfolio_options['contact_title']:'Contact Details'  ?></h3>
			                        <div class="title-divider"></div>
			                        <div class="row">
			                        	<div class="col-xs-12 col-sm-12 col-md-12">
			                            	<div class="row">
						                        <?php if($portfolio_options['email_id'])
						                        		{
						                                    echo '<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo '<div class="info-title">Email:</div>';
						                                    echo '</div>';
						                                    echo '<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo '<div class="info"><a href="mailto:'.$portfolio_options['email_id'].'">'.$portfolio_options['email_id'].'</a></div>';
						                                   		echo '</div>';
													} 
												
												 	if($portfolio_options['primary_ph'])
						                       		 	{   
						                            
						                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo'<div class="info-title">Primary Phone:</div>';
						                                    echo'</div>';
						                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo'<div class="info"><a href="tel:'.$portfolio_options['primary_ph'].'">'.$portfolio_options['primary_ph'].'</a></div>';
						                                    echo'</div>';
						                                }    
						                         
						                         	if($portfolio_options['alternate_ph'])
						                       			{   
						                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo'<div class="info-title">Alternate Phone:</div>';
						                                    echo'</div>';
						                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                        echo'<div class="info"><a href="tel:'.$portfolio_options['alternate_ph'].'">'.$portfolio_options['alternate_ph'].'</a></div>';
						                                    echo'</div>';
						                            
						                        		}   
						                        
						                        	if($portfolio_options['fax_no'])
						                        		{  
						                            
						                                     echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                         echo'<div class="info-title">Fax Number:</div>';
						                                     echo'</div>';
						                                     echo'<div class="col-xs-6 col-sm-6 col-md-6">';
						                                         echo'<div class="info"><a href="tel:'.$portfolio_options['fax_no'].'">'.$portfolio_options['fax_no'].'</a></div>';   
						                                     echo'</div>';
						                                 
						                        		} ?>
			                        		</div>
			                        	</div>
			                        </div>
		                    	</div>
							<?php }
                    
		                    if($portfolio_options['ofc_hrs_checkbox'] == 1)
		                    { ?>
			                    <div class="office-time wow fadeInRight" data-wow-delay="1s">
			                        <h3><?php echo $portfolio_options['ofc_hrs_title']? $portfolio_options['ofc_hrs_title']:'Office Hours'  ?></h3>
			                        <div class="title-divider"></div>
			                        <div class="row">
			                            <div class="col-xs-12 col-sm-12 col-md-12">
			                                <div class="row">
			                                <?php if($portfolio_options['weekday'])
					                                {
														echo'<div class="col-xs-6 col-sm-6 col-md-6">';
					                                        echo'<div class="info">Monday - Friday</div>';
					                                    echo'</div>';
					                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
					                                        echo'<div class="info">'.$portfolio_options['weekday'].'</div>';
					                                    echo'</div>';
													}
					                                if($portfolio_options['weekend'])
					                                {    
					                               
					                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
					                                        echo'<div class="info">Saturday - Sunday</div>';
					                                    echo'</div>';
					                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
					                                        echo'<div class="info">'.$portfolio_options['weekend'].'</div>';
					                                    echo'</div>';
					                                }
			                            			if($portfolio_options['holiday'])
			                            			{
														echo'<div class="col-xs-6 col-sm-6 col-md-6">';
					                                        echo'<div class="info">Holidays</div>';
					                                    echo'</div>';
					                                    echo'<div class="col-xs-6 col-sm-6 col-md-6">';
					                                        echo'<div class="info">'.$portfolio_options['holiday'].'</div>';
					                                    echo'</div>';
													}
													?>
					                            
			                        		</div>
			                        		<p>* Feel free to email or call us after hours.</p>
			                    		</div>
			                    	</div>
			                    </div>
			                <?php } ?>
                </div>
            </article>
        </section>
    </section>
</div>

<!-- google map section -->
    <div id="map_canvas"></div>
<!-- google map section end -->



<!-- google map API section -->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;language=en"></script>
<script type="text/javascript">

function initialize() {
    var image = "<?php echo $marker_img_url; ?>";
    var mapOptions = {
        zoom: 13,
        center: new google.maps.LatLng("<?php echo $marker_latitude; ?>", "<?php echo $marker_longitude; ?>"),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    var myPos = new google.maps.LatLng("<?php echo $marker_latitude; ?>", "<?php echo $marker_longitude; ?>");
    
    var myMarker = new google.maps.Marker({
        position: myPos,
        map: map,
        draggable: false,
        icon: image
    });
    
	var infowindow =  new google.maps.InfoWindow({
		content: ''
	});
		
	bindInfoWindow(myMarker, map, infowindow, "<?php echo $marker_text; ?>");

}

google.maps.event.addDomListener(window, 'resize', initialize);
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<!-- google map API section end -->


<?php 
endforeach; 
wp_reset_postdata();
get_footer(); ?>