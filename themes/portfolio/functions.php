<?php
/**
   Portfoliotheme functions and definitions
 * @package Portfolio
 * @author Purplethemes
 */

define('THEME_DIR', get_template_directory());
define('THEME_URI', get_template_directory_uri());
define('SITE_URL', site_url());

 
define('LIBS_DIR', THEME_DIR. '/inc');
define('LIBS_URI', THEME_URI. '/inc');
define('LANG_DIR', THEME_DIR. '/languages');

define('THEME_VERSION', '1.0');

/* ---------------------------------------------------------------------------
 * Loads Theme Files
* --------------------------------------------------------------------------- */
 
 
// Loads Theme Functions -----------------------------------------------------------------------
require_once( THEME_DIR. '/functions/theme-functions.php' );

// Load Header Theme Fucntion -----------------------------------------------------------------------
require_once( THEME_DIR. '/functions/theme-head.php' );

// Load Theme Layout -----------------------------------------------------------------------
require_once( THEME_DIR. '/functions/theme-layouts.php' );

// Load Theme Menu -------------------------------------------------------------------------
require_once( THEME_DIR. '/functions/theme-menu.php' );

// Load Theme Shortcode -------------------------------------------------------------------------
require_once( THEME_DIR. '/functions/theme-shortcodes.php' );

// Load Theme Shortcode -------------------------------------------------------------------------
require_once( THEME_DIR. '/functions/class-tgm-plugin-activation.php' );

/**
 * Set up the content width value based on the theme's design.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 474;
}

/**
 * Portfolio theme only works in WordPress 3.6 or later.
 */
/*if ( version_compare( $GLOBALS['wp_version'], '3.6', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}
*/

/**
 * Adjust content_width value for image attachment template.
 */
function portfoliotheme_content_width() {
	if ( is_attachment() && wp_attachment_is_image() ) {
		$GLOBALS['content_width'] = 810;
	}
}
add_action( 'template_redirect', 'portfoliotheme_content_width' );

/**
 * Getter function for Featured Content Plugin.
 *
 * @return array An array of WP_Post objects.
 */
function portfoliotheme_get_featured_posts() {
	/**
	 * Filter the featured posts to return in Portfolio theme.
	 *
	 * @param array|bool $posts Array of featured posts, otherwise false.
	 */
	return apply_filters( 'portfoliotheme_get_featured_posts', array() );
}

/**
 * A helper conditional function that returns a boolean value.
 * @return bool Whether there are featured posts.
 */
function portfoliotheme_has_featured_posts() {
	return ! is_paged() && (bool) portfoliotheme_get_featured_posts();
}

/**
 * Register three Portfolio theme widget areas.
 */
function portfoliotheme_widgets_init() {
	//require get_template_directory() . '/inc/widgets.php';
	//register_widget( 'portfoliotheme_Ephemera_Widget' );

	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'wpt' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Main sidebar.', 'wpt' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '<div class="title-divider"></div></h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'wpt' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Additional sidebar.', 'wpt' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '<div class="title-divider"></div></h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'wpt' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears in the footer section of the site.', 'wpt' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'portfoliotheme_widgets_init' );
add_filter('widget_text', 'do_shortcode');
/**
 * Register Lato Google font for Portfolio theme.
 * @return string
 */
function portfoliotheme_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Lato, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Lato font: on or off', 'wpt' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Lato:300,400,700,900,300italic,400italic,700italic' ), "//fonts.googleapis.com/css" );
	}

	return $font_url;
}

/**
 * Enqueue scripts and styles for the front end.
 */
function portfoliotheme_scripts() {
    
	// Add Lato font, used in the main stylesheet.
	wp_enqueue_style( 'portfoliotheme-lato', portfoliotheme_font_url(), array(), null );

	
	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'portfoliotheme-ie', get_template_directory_uri() . '/css/ie.css', array( 'portfoliotheme-style', 'genericons' ), '20131205' );
 
    
	wp_style_add_data( 'portfoliotheme-ie', 'conditional', 'lt IE 9' );
    
    
    
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'portfoliotheme-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		wp_enqueue_script( 'jquery-masonry' );
	}

	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
		wp_enqueue_script( 'portfoliotheme-slider', get_template_directory_uri() . '/js/slider.js', array( 'jquery' ), '20131205', true );
		wp_localize_script( 'portfoliotheme-slider', 'featuredSliderDefaults', array(
			'prevText' => __( 'Previous', 'wpt' ),
			'nextText' => __( 'Next', 'wpt' )
		) );
	}

}
add_action( 'wp_enqueue_scripts', 'portfoliotheme_scripts' );

/**
 * Enqueue Google fonts style to admin screen for custom header display.
 */
function portfoliotheme_admin_fonts() {
	wp_enqueue_style( 'portfoliotheme-lato', portfoliotheme_font_url(), array(), null );
}
add_action( 'admin_print_scripts-appearance_page_custom-header', 'portfoliotheme_admin_fonts' );

if ( ! function_exists( 'portfoliotheme_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 */
function portfoliotheme_the_attached_image() {
	$post                = get_post();
	/**
	 * Filter the default Portfolio theme attachment size.
	 *
	 * @param array $dimensions {
	 *     An array of height and width dimensions.
	 *
	 *     @type int $height Height of the image in pixels. Default 810.
	 *     @type int $width  Width of the image in pixels. Default 810.
	 * }
	 */
	$attachment_size     = apply_filters( 'portfoliotheme_attachment_size', array( 810, 810 ) );
	$next_attachment_url = wp_get_attachment_url();

	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID',
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		// get the URL of the next image attachment...
		if ( $next_id ) {
			$next_attachment_url = get_attachment_link( $next_id );
		}

		// or get the URL of the first image attachment.
		else {
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
		}
	}

	printf( '<a href="%1$s" rel="attachment">%2$s</a>',
		esc_url( $next_attachment_url ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;

if ( ! function_exists( 'portfoliotheme_list_authors' ) ) :
/**
 * Print a list of all site contributors who published at least one post.
 */
function portfoliotheme_list_authors() {
	$contributor_ids = get_users( array(
		'fields'  => 'ID',
		'orderby' => 'post_count',
		'order'   => 'DESC',
		'who'     => 'authors',
	) );

	foreach ( $contributor_ids as $contributor_id ) :
		$post_count = count_user_posts( $contributor_id );

		// Move on if user has not published a post (yet).
		if ( ! $post_count ) {
			continue;
		}
	?>

	<div class="contributor">
		<div class="contributor-info">
			<div class="contributor-avatar"><?php echo get_avatar( $contributor_id, 132 ); ?></div>
			<div class="contributor-summary">
				<h2 class="contributor-name"><?php echo get_the_author_meta( 'display_name', $contributor_id ); ?></h2>
				<p class="contributor-bio">
					<?php echo get_the_author_meta( 'description', $contributor_id ); ?>
				</p>
				<a class="button contributor-posts-link" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>">
					<?php printf( _n( '%d Article', '%d Articles', $post_count, 'wpt' ), $post_count ); ?>
				</a>
			</div><!-- .contributor-summary -->
		</div><!-- .contributor-info -->
	</div><!-- .contributor -->

	<?php
	endforeach;
}
endif;

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image except in Multisite signup and activate pages.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function portfoliotheme_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} elseif ( ! in_array( $GLOBALS['pagenow'], array( 'wp-activate.php', 'wp-signup.php' ) ) ) {
		$classes[] = 'masthead-fixed';
	}

	if ( is_archive() || is_search() || is_home() ) {
		$classes[] = 'list-view';
	}

	if ( ( ! is_active_sidebar( 'sidebar-2' ) )
		|| is_page_template( 'page-templates/full-width.php' )
		|| is_page_template( 'page-templates/contributors.php' )
		|| is_attachment() ) {
		$classes[] = 'full-width';
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		$classes[] = 'footer-widgets';
	}

	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}

	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
		$classes[] = 'slider';
	} elseif ( is_front_page() ) {
		$classes[] = 'grid';
	}

	return $classes;
}
add_filter( 'body_class', 'portfoliotheme_body_classes' );

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function portfoliotheme_post_classes( $classes ) {
	if ( ! post_password_required() && ! is_attachment() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}

	return $classes;
}
add_filter( 'post_class', 'portfoliotheme_post_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function portfoliotheme_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'wpt' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'portfoliotheme_wp_title', 10, 2 );


/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own Featured_Content class on or
 * before the 'setup_theme' hook.
 */
if ( ! class_exists( 'Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {
	//require get_template_directory() . '/inc/featured-content.php';
    
// Hide admin toolbar
add_filter('show_admin_bar', '__return_false');
}

function remove_comment_fields($fields) {
   
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields', 'remove_comment_fields');