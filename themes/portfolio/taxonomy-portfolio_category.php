<?php
/**
 * Taxonomy Portfolio categories
 *
 * @package Portfolio
 * @author Purplethemes
 */
 
get_header(); 

global $wp_query, $portfolio_options;
$post_id = $wp_query->get_queried_object_id();
$current_post_id = $post_id + 1;
$portfolio_post_count = ( ! empty($portfolio_options['no_of_portfolio_posts'])) ? $portfolio_options['no_of_portfolio_posts'] : '8';

switch ( get_post_meta($post_id, 'Layout', true) ) {
	case 'left-sidebar':
		$class = ' left';
		break;
	case 'right-sidebar':
		$class = 'right';
		break;
	default:
		$class = '';
		break;
}

 
if($class == 'left'){
  
    $right_class = 'col-xs-12 col-sm-9 col-md-9 pull-right';
    $left_class = 'col-xs-12 col-sm-3 col-md-3 pull-left';
    $class = 'left';
}
     
elseif($class == 'right'){
    
    $right_class = 'col-xs-12 col-sm-9 col-md-9';
    $left_class = 'col-xs-12 col-sm-3 col-md-3';
    $class = 'right';
}
     
else{
    
    $class = '';
}

?>

<div class="container">
    <article class="row"> 
        <section class="project-section">  

			<!-- .select_category -->
			<?php
			
			if($class) echo'<article class="' .$right_class.'">'; 
                    
	                if( $portfolio_categories= get_terms('portfolio_category') ){
	                    echo '<div class="portfolioFilter">';
	        				foreach( $portfolio_categories as $category ){
	                            if( $category->term_taxonomy_id == $current_post_id ) {
								    echo '<a href="'.get_term_link($category).'" class="nav-all current">'.$category->name.'</a>';    
								}
	                            else {
	                                echo '<a href="'.get_term_link($category).'" class="nav-all">'.$category->name.'</a>';        
	                            }  
	                                  
	                        }
	                    echo '</div>'; 
	                } 
	                    
	                //wp_list_categories( $menu_args ); 

				    $args = array( 
	        			'post_type' => 'portfolio',
	        			'posts_per_page' => $portfolio_options[ 'no_of_portfolio_posts']?  $portfolio_options[ 'no_of_portfolio_posts']:'8' ,
	        			'paged' => ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1,
	        			'order' => $portfolio_options['pf_item_order'] ? $portfolio_options['pf_item_order']: 'ASC',
	        			'orderby' => $portfolio_options['pf_item_order_column'] ? $portfolio_options['pf_item_order_column']:'date',
	        			'taxonomy' => 'portfolio_category',
	        		);
							
	        		global $query_string;
	        		parse_str( $query_string, $qstring_array );
	        		$query_args = array_merge( $args,$qstring_array );
	        		               
	        		$temp = $wp_query;
	        		$wp_query = null;
	        		$wp_query = new WP_Query();
	        		$wp_query->query( $query_args );
					
	                if ($wp_query->have_posts()) {
		                 		
					 	echo '<div class="grid">';
							
						       while ( have_posts() )
								{
									the_post();
							        get_template_part( 'libs/content', 'portfolio' );
							    }
		                     
	                	echo '</div>';
						
						echo '<article class="col-xs-12 col-sm-12 col-md-12 text-right">';
							echo '<ul class="pagination wow fadeInUp" data-wow-delay="0.3s">';
							    //wpt_pagination('portfolio',$portfolio_post_count); 
		                        wpt_pagination();
							echo '</ul>';
						echo '</article>';
			        }//end if
			     
			    wp_reset_query(); 
				$wp_query = $temp;
				the_post();
		    
            echo '</article>'; 
			 	
            if($class){ 
                echo '<article class="' .$left_class. '">';
                    echo'<aside>';
                        echo'<div class="sidebar">';
                            get_sidebar();
                        echo '</div>';
                    echo '</aside>';
                echo'</article>';
        
            } 
		?>
	
        </section> <!--project-section end-->
    </article>
</div>

<?php get_footer(); ?>