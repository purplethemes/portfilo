<!-- image slider section -->
<div class="slider-banner">
    <ul class="slides">
        
        <?php
        $args = array( 'post_type' => 	'slide',);
		$my_query = new WP_Query( $args );
		
        if( $my_query->have_posts() )  {
           
                while ($my_query->have_posts()) : $my_query->the_post();

                    $sc_title = get_post_meta( get_the_ID(), 'slider_content_title', true);
                    $sc_text = get_post_meta( get_the_ID(), 'slider_content_text', true);     
                    $sb_text = get_post_meta( get_the_ID(), 'slider_button_text', true);
                    $sb_link = get_post_meta( get_the_ID(), 'slider_button_link', true); 
                   	$sl_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'full', false);
                    $sl_imgURL = $sl_imgArray[0];
					
					if(!empty($sl_imgURL)) { ?>
						<li style="background-image: url('<?php echo $sl_imgURL ?>');" class="slide">
                   <?php } else { ?>
				   		<li style="background-image: url('<?php echo THEME_URI. '/images/no-img-portfolio.jpg' ?>');" class="slide">
				   <?php } ?>
	                    <div class="slider-overlay"></div>
	                    <div class="info" style="margin-top: 0px; opacity: 1;">
	                        <h2><?php echo $sc_title; ?></h2>
	                        <p><?php echo $sc_text; ?></p>
	                        <a class="btn-white" href="<?php echo $sb_link; ?>"><?php echo $sb_text; ?></a>
	                        
	                    </div>
                    </li>
                <?php 
                endwhile; 
            } 
                ?>
            
    </ul>
        
    <ul class="flex-direction-nav">
        
        <li><a href="#" class="flex-prev">Previous</a></li>
           
        <li><a href="#" class="flex-next">Next</a></li>
            
    </ul>
</div>
    
<!-- image slider section end -->
