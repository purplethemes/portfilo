<?php
/* homepage content */
?>

<?php global $portfolio_options;

$sec1_title = $portfolio_options['hp_section1_title'];
$sec_content = $portfolio_options['hp_section_content'];
$pf_title = $portfolio_options['portfolio_section_title'];
$pf_content = $portfolio_options['portfolio_section_content'];


if($portfolio_options['homepage_checkbox'] == 1){ ?>
	<!-- our services section-->
		<section class="our_service">
		    <div class="title-area wow fadeIn">
		        <?php if ($portfolio_options['hp_section1_title']) : ?><h2 class="section-title"><?php echo $portfolio_options['hp_section1_title']; ?></h2>
		        <?php else : ?><h2 class="section-title"><?php _e('', 'wpt'); ?></h2><?php endif; ?>
		        <div class="section-divider divider-inside-top"></div>
		        <?php if ( $portfolio_options['hp_section_content'] ) : ?><p class="section-sub-text"><?php echo $portfolio_options['hp_section_content']; ?></p>
		        <?php else : ?><p class="section-sub-text"><?php _e('', 'wpt'); ?></p><?php endif; ?>
		    </div>
		                
		    <article class="service-list">
		        <section class="row">
		        <?php 
		            
		            $cnt = 0;

		            for($i=1; $i<=3 ;$i++){
		                if(!empty($portfolio_options['hp_subsection'.$i.'_content']))
		                {
		                    $cnt=$cnt+1;
		                }
		            }
		            
		            if( $cnt == 1 ):
		              $col = " col-sm-12 col-md-12";
		            elseif( $cnt == 2 ):
		              $col = " col-sm-6 col-md-6";
		            else:
		              $col = " col-sm-4 col-md-4";
		            endif;  
		            
		            for($i=1; $i<=3; $i++)
		                {
		                    if(!empty($portfolio_options['hp_subsection'.$i.'_content']))
		                    {
		                        echo '<div class="service-container wow fadeInUp" data-wow-delay="0.4s">';
		                        echo '<article class="col-xs-12'. $col .'">';
		                        echo $portfolio_options['hp_subsection'.$i.'_content'];
		                        echo '</article>';
		                        echo '</div>';
		                    }
		                }
		            
		            ?>
		                      
		        </section>
		    </article>
		</section> <!-- our services section end -->
<?php } ?>




<!-- recent project section-->

<?php if( $portfolio_options['portfolio_checkbox'] == 1 ) {?> 
		<section class="recent_project">
			<div class="title-area wow  fadeIn">
				
				<?php if($portfolio_options['portfolio_section_title']) { ?><h2 class="section-title"><?php echo $portfolio_options['portfolio_section_title']; ?></h2><?php } ?>
	            <div class="section-divider divider-inside-top"></div>
	            <?php if($portfolio_options['portfolio_section_content']) { ?><p class="section-sub-text"><?php echo $portfolio_options['portfolio_section_content']; ?></p><?php } ?>
	            <?php echo do_shortcode( '[portfolio count="4"]' ); ?>
	            
			</div>           
        </section>   
			<?php } 

if( $portfolio_options['gallery_checkbox'] == 1  ) {?> 
		
		<section class="recent_project">		
				
			<div class="title-area wow  fadeIn">	
			
			    <?php if($portfolio_options['gallery_section_title']) { ?><h2 class="section-title"><?php echo $portfolio_options['gallery_section_title']; ?></h2><?php } ?>
			    <div class="section-divider divider-inside-top"></div>
			    <?php if($portfolio_options['gallery_section_content']) { ?><p class="section-sub-text"><?php echo $portfolio_options['gallery_section_content']; ?></p><?php } ?>
			    <?php echo do_shortcode( '[gallery count="4"]' ); ?>
			     
			</div>
		</section>	        
			<?php } 

if( $portfolio_options['testimonial_checkbox'] == 1  ) {?> 
		
			    <?php echo do_shortcode( '[testimonial count="4"]' ); ?>
			     
		        
			<?php } 		
			
			
			 //echo do_shortcode( '[counter]' ); ?>
				  
			 
