<?php
/**
	ReduxFramework Sample Config File
	For full documentation, please visit: https://github.com/ReduxFramework/ReduxFramework/wiki
**/

if ( !class_exists( "ReduxFramework" ) ) {
	return;
} 


if ( !class_exists( "Portfolio_Redux_Framework_config" ) ) {
	class Portfolio_Redux_Framework_config {

		public $args = array();
		public $sections = array();
		public $theme;
		public $ReduxFramework;

		public function __construct( ) {
			$this->theme = wp_get_theme();
			$this->setArguments();
			$this->setSections();
			if ( !isset( $this->args['opt_name'] ) ) { // No errors please
				return;
			}
			$this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
		}


		/**
			All the possible arguments for Redux.
			For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
		 **/
		public function setArguments() {
			$theme = wp_get_theme(); // For use with some settings. Not necessary.
			$this->args = array(
	            // TYPICAL -> Change these values as you need/desire
				'opt_name'          	=> 'portfolio_options', // This is where your data is stored in the database and also becomes your global variable name.
				'display_name'			=> $theme->get('Name'), // Name that appears at the top of your panel
				'display_version'		=> $theme->get('Version'), // Version that appears at the top of your panel
				'menu_type'          	=> 'menu', //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
				'allow_sub_menu'     	=> true, // Show the sections below the admin menu item or not
				'menu_title'			=> __( 'Portfolio Options', 'wpt' ),
	            'page'		 	 		=> __( 'Portfolio Options', 'wpt' ),
	            'google_api_key'   	 	=> '', // Must be defined to add google fonts to the typography module
	            'global_variable'    	=> '', // Set a different name for your global variable other than the opt_name
	            'dev_mode'           	=> false, // Show the time the page took to load, etc
	            'customizer'         	=> true, // Enable basic customizer support
	            // OPTIONAL -> Give you extra features
	            'page_priority'      	=> null, // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
	            'page_parent'        	=> 'themes.php', // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	            'page_permissions'   	=> 'manage_options', // Permissions needed to access the options panel.
	            'menu_icon'          	=> LIBS_URI. '/redux/ReduxCore/assets/img/admin.png', // Specify a custom URL to an icon
	            'last_tab'           	=> '', // Force your panel to always open to a specific tab (by id)
	            'page_icon'          	=> 'icon-themes', // Icon displayed in the admin panel next to your menu_title
	            'page_slug'          	=> '_options', // Page slug used to denote the panel
	            'save_defaults'      	=> true, // On load save the defaults to DB before user clicks save or not
	            'default_show'       	=> false, // If true, shows the default value next to each field that is not the default value.
	            'default_mark'       	=> '', // What to print by the field's title if the value shown is default. Suggested: *
	            // CAREFUL -> These options are for advanced use only
	            'transient_time' 	 	=> 60 * MINUTE_IN_SECONDS,
	            'output'            	=> true, // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
	            'output_tab'            => true, // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
	            //'domain'             	=> 'redux-framework', // Translation domain key. Don't change this unless you want to retranslate all of Redux.
	            'footer_credit'      	=> ' ', // Disable the footer credit of Redux. Please leave if you can help it.
	            // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
	            'database'           	=> '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
	            'show_import_export' 	=> true, // REMOVE
	            'system_info'        	=> false, // REMOVE
	            'allow_tracking'        => false, // REMOVE
	            'help_tabs'          	=> array(),
	            'help_sidebar'       	=> '', // __( '', $this->args['domain'] );            
				);
			// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.		
			$this->args['share_icons'][] = array(
			    'url' => '', //http://themeforest.net/user/ab-themes
			    'title' => 'Visit us on TeamForest', 
			    'icon' => 'el-icon-leaf'
			    // 'img' => '', // You can use icon OR img. IMG needs to be a full URL.
			);		
			$this->args['share_icons'][] = array(
			    'url' => '', //http://twitter.com/ab_themes_com
			    'title' => 'Follow us on Twitter', 
			    'icon' => 'el-icon-twitter'
			);
		}


		/**
			Sections and fields declaration
		 **/
		public function setSections() {
            
                $this->sections[] = array(
    				'title' => __('General', 'wpt'),
    				'icon' => 'el-icon-cogs',
    				'fields' => array(
                        
                        array(
    						'id'          => 'favicon',
    						'title'       => __('Favicon', 'wpt'),
    						'desc'        => __('Upload a 16px x 16px ico image that will represent your website favicon.', 'wpt'),
    						'type'        => 'media',
    					),
    					array(
    						'id'          => 'logo',
    						'title'       => __('Logo', 'wpt'),
                            'url'         => false,
                            'width'       => 150,
                            'height'      => 150,
                            'thumbnail'    => true,
    						'desc'        => __('Change the logo image.', 'wpt'),
    						'type'        => 'media',
    					),
                        array(
    						'id'          => 'site_title',
    						'title'       => __('Site Title', 'wpt'),
                            'desc'        => __('Site Title and Tagline. Setup title & tagline.', 'wpt'),
    						'type'        => 'text',
    					),
                        array(
    						'id'          => 'site_desc',
    						'title'       => __('Site Description', 'wpt'),
                            'desc'        => __('The site description/tagline under site title.', 'wpt'),
    						'type'        => 'textarea',
    					),
                        array(
    						'id'          => 'hide_comments',
    						'title'       => __('Hide Comments', 'wpt'),
    						'desc'        => __('Check this to hide WordPress commenting system.', 'wpt'),
    						'type'        => 'checkbox',
    					),
    					array(
    						'id'          => 'hide_author_bio',
    						'title'       => __('Hide Author Bio', 'wpt'),
    						'desc'        => __('Check this to hide author biography under post content.', 'wpt'),
    						'type'        => 'checkbox',
    					),
                        array(
    						'id'          => 'hide_breadcrumbs_from_bar',
    						'title'       => __('Hide Breadcrumbs', 'wpt'),
    						'type'        => 'checkbox',
    					),
                        array(
    						'id'          => 'custom_css',
    						'title'       => __('Custom CSS', 'wpt'),
    						'desc'        => __('Here you can place additional CSS or CSS to override theme\'s styles.', 'wpt'),
    						'type'        => 'textarea',
    						'validate' => 'css',
    						'type' => 'ace_editor',
    						'mode' => 'css',
    			            'theme' => 'monokai',
    					),
                        array(
    						'id'          => 'analytics_code',
    						'title'       => __('Analytics Code', 'wpt'),
    						'desc'        => __('Here you can paste Google Analytics (or similar, html valid) code to be printed out on every page just before closing body tag.', 'wpt'),
    						'type'        => 'textarea',
    						'type' => 'ace_editor',
    						'mode' => 'javascript',
    			            'theme' => 'monokai',
    					),
                        array(
    						'id'          => 'footer_text',
    						'title'       => __('Footer text', 'wpt'),
    						'desc'        => __('Set your theme footer text here.', 'wpt'),
    						'type'        => 'editor',
    					),
    					
    				)
    			);

				$this->sections[] = array(
					'title' => __('Home Page Settings', 'wpt'),
					'icon' => 'el-icon-home',
					'fields' => array(
	                    array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('', 'wpt'),
					       'indent' => true 
					   ),
	                   array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Home Page Section', 'wpt'),
					       'indent' => true 
					   ),
	                   
	                    array(
	                        'id'        => 'homepage_checkbox',
	                        'type'      => 'checkbox',
	                        'title'     => __('Show Home Page Section', 'wpt'),
	                        'subtitle'  => __('Check this to enable Home Page section.', 'wpt'),
	                        'default'   => '1',
	                        
	                    ),
	                   
	                   array(
							'id'          => 'hp_section1_title',
							'title'       => __('Section Title', 'wpt'),
	                        'desc'        => __('Specify the title for the section on Home Page.', 'wpt'),
							'type'        => 'text',
							'required'    => array('homepage_checkbox','equals','1'),
						),

	                     array(
							'id'          => 'hp_section_content',
							'title'       => __('Section Content', 'wpt'),
							'desc'        => __('Specify the content for section on Home Page.', 'wpt'),
							'type'        => 'editor',
							'required'    => array('homepage_checkbox','equals','1'),
						),   
	                    
	                     array(
							'id'          => 'hp_subsection1_content',
							'title'       => __('Sub-Section-1 Content', 'wpt'),
							'desc'        => __('Specify the content for first sub-section on Home Page.', 'wpt'),
							'type'        => 'editor',
							'required'    => array('homepage_checkbox','equals','1'),
						), 
	                   
	                     array(
							'id'          => 'hp_subsection2_content',
							'title'       => __('Sub-Section-2 Content', 'wpt'),
							'desc'        => __('Specify the content for second sub-section on Home Page.', 'wpt'),
							'type'        => 'editor',
							'required'    => array('homepage_checkbox','equals','1'),
						),  
	                    
	                    array(
							'id'          => 'hp_subsection3_content',
							'title'       => __('Sub-Section-3 Content', 'wpt'),
							'desc'        => __('Specify the content for third sub-section on Home Page.', 'wpt'),
							'type'        => 'editor',
							'required'    => array('homepage_checkbox','equals','1'),
						),  
	                   
	                   	array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Portfolio Section', 'wpt'),
					       'indent' => true 
					   ),
	                   
	                    array(
	                        'id'        => 'portfolio_checkbox',
	                        'type'      => 'checkbox',
	                        'title'     => __('Show Portfolio Section', 'wpt'),
	                        'subtitle'  => __('Check this to enable portfolio section.', 'wpt'),
	                        'default'   => '1',
	                        
	                    ),
	                   
	                    array(
							'id'          => 'portfolio_section_title',
							'title'       => __('Portfolio Title', 'wpt'),
	                        'desc'        => __('Specify the title for the portfolio on home page.', 'wpt'),
							'type'        => 'text',
	                        'required'    => array('portfolio_checkbox','equals','1'),
						),   
	                   
	                    array(
							'id'          => 'portfolio_section_content',
							'title'       => __('Portfolio Content', 'wpt'),
							'desc'        => __('Specify the content for portfolio on home page.', 'wpt'),
							'type'        => 'editor',
	                        'required'    => array('portfolio_checkbox','equals','1'),
						),        
	                   
	                    array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Gallery Section', 'wpt'),
					       'indent' => true 
					   ),
	                   
	                     array(
							'id'          => 'gallery_checkbox',
							'title'       => __('Show Gallery Section', 'wpt'),
							'subtitle'    => __('Check this to enable gallery section.', 'wpt'),
							'type'        => 'checkbox',
	                        'default'     => '1',
	                    ),
	                    
	                    array(
							'id'          => 'gallery_section_title',
							'title'       => __('Gallery Title', 'wpt'),
	                        'desc'        => __('Specify the title for the gallery on home page.', 'wpt'),
							'type'        => 'text',
	                        'required' => array('gallery_checkbox','equals','1'),
						),   
	                   
	                    array(
							'id'          => 'gallery_section_content',
							'title'       => __('Gallery Content', 'wpt'),
							'desc'        => __('Specify the content for gallery on home page.', 'wpt'),
							'type'        => 'editor',
	                        'required' => array('gallery_checkbox','equals','1'),
						),   
						
						array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Testimonial Section', 'wpt'),
					       'indent' => true 
					   ),  
					    
					    array(
							'id'          => 'testimonial_checkbox',
							'title'       => __('Show Testimonial Section', 'wpt'),
							'subtitle'    => __('Check this to enable testimonial section.', 'wpt'),
							'type'        => 'checkbox',
	                        'default'     => '1',
	                    ),
					    
					    array(
							'id'          => 'testimonial_section_title',
							'title'       => __('Testimonial Title', 'wpt'),
	                        'desc'        => __('Specify the title for the testimonial section.', 'wpt'),
							'type'        => 'text',
							'required' => array('testimonial_checkbox','equals','1'),
	                        
						),  
						 
						 array(
							'id'          => 'testimonial_section_content',
							'title'       => __('Testimonial Content', 'wpt'),
							'desc'        => __('Specify the content for testimonial on home page.', 'wpt'),
							'type'        => 'editor',
	                        'required' => array('testimonial_checkbox','equals','1'),
						),      
	                   
	                ) 
				);

	            $this->sections[] = array(
					'title' => __('Portfolio Page', 'wpt'),
					'icon' => 'el-icon-folder-open',
					'fields' => array(
	                   
	                   array(
							'id'          => 'no_of_portfolio_posts',
							'title'       => __('Number of Post', 'wpt'),
	                        'desc'        => __('Specify the number of post to be displayed per page.', 'wpt'),
							'type'        => 'text',
						),
	                    
	                    array(
	                        'id'       => 'portfolio_post_layout_select',
	                        'type'     => 'image_select',
	                        'title'    => __('Portfolio Post Layout', 'wpt'), 
	                        'desc'     => __('Layout for portfolio items list. Choose between 2, 3 or 4 column layout.', 'wpt'),
	                    
	                        //Must provide key => value pairs for radio options 
	                        
	                        'options'  => array(
	                        '1'      => array(
	                            'alt'   => 'Two columns', 
	                            'img'   => LIBS_URI. '/redux/ReduxCore/assets/img/two-column.png'
	                            
	                        ),
	                        '2'      => array(
	                            'alt'   => 'Three columns', 
	                            'img'   => LIBS_URI. '/redux/ReduxCore/assets/img/three-column.png'
	                        ),
	                        '3'      => array(
	                            'alt'   => 'Four columns', 
	                            'img'  => LIBS_URI. '/redux/ReduxCore/assets/img/four-column.png'
	                            
	                        ),
	                        
	                    ),
	                    'default' => '2'
	                    ),   
	                    array(
	                        'id'       => 'pf_item_order_column',
	                        'type'     => 'select',
	                        'title'    => __('Order By', 'wpt'),
	                        'desc'     => __('Portfolio item order by column', 'wpt'),
	                        'options'  => array('menu_order' => 'Manual Order','date' => 'Date','title' => 'Title'),
	                        'default'  => 'menu_order',
	                    ),
	                    
	                    array(
	                        'id'       => 'pf_item_order',
	                        'type'     => 'select',
	                        'title'    => __('Order', 'wpt'),
	                        'desc'     => __('Portfolio items order', 'wpt'),
	                        'options'  => array('ASC' => 'Ascending','DESC' => 'Descening'),
	                        'default'  => 'ASC',
	                    ),
	                    
	                 ) 
				);
	            
	            $this->sections[] = array(
					'title' => __('Gallery Page', 'wpt'),
					'icon' => 'el-icon-picture',
					'fields' => array(
	                    array(
							'id'          => 'no_of_gallery_posts',
							'title'       => __('Number of Post', 'wpt'),
	                        'desc'        => __('Specify the number of post to be displayed per page.', 'wpt'),
							'type'        => 'text',
						),
	                    array(
	                        'id'       => 'gallery_post_layout_select',
	                        'type'     => 'image_select',
	                        'title'    => __('Gallery Post Layout', 'wpt'), 
	                        'desc'     => __('Layout for gallery items list. Choose between 2, 3 or 4 column layout.', 'wpt'),
	                    
	                        //Must provide key => value pairs for radio options 
	                        
	                        'options'  => array(
	                        '1'      => array(
	                            'alt'   => 'Two columns', 
	                            'img'   => LIBS_URI. '/redux/ReduxCore/assets/img/two-column.png'
	                            
	                        ),
	                        '2'      => array(
	                            'alt'   => 'Three columns', 
	                            'img'   => LIBS_URI. '/redux/ReduxCore/assets/img/three-column.png'
	                        ),
	                        '3'      => array(
	                            'alt'   => 'Four columns', 
	                            'img'  => LIBS_URI. '/redux/ReduxCore/assets/img/four-column.png'
	                            
	                        ),
	                        
	                    ),
	                    'default' => '2'
	                    ),   
	                   
	                    array(
	                        'id'       => 'gallery_item_order_column',
	                        'type'     => 'select',
	                        'title'    => __('Order By', 'wpt'),
	                        'desc'     => __('Gallery item order by column', 'wpt'),
	                        'options'  => array('menu_order' => 'Manual Order','date' => 'Date','title' => 'Title'),
	                        'default'  => 'menu_order',
	                    ),
	                    
	                    array(
	                        'id'       => 'gallery_item_order',
	                        'type'     => 'select',
	                        'title'    => __('Order', 'wpt'),
	                        'desc'     => __('Gallery items order', 'wpt'),
	                        'options'  => array('ASC' => 'Ascending','DESC' => 'Descening'),
	                        'default'  => 'ASC',
	                    ),
	                    
	                 ) 
				);
				
				
				$this->sections[] = array(
					'title' => __('Counter Section', 'wpt'),
					'icon' => 'el-icon-tasks',
					'fields' => array(
						
						 array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Counter Section 1', 'wpt'),
					       'indent' => true 
					   ),
	                   
	                     array(
							'id'          => 'counter_checkbox_1',
							'title'       => __('Show Counter Section 1', 'wpt'),
							'subtitle'    => __('Check this to enable first counter section.', 'wpt'),
							'type'        => 'checkbox',
	                        'default'     => '1',
	                    ),
					
	                    array(
							'id'          => 'counter_title_1',
							'title'       => __('Title 1:', 'wpt'),
	                        'desc'        => __('Specify the title for first counter section.', 'wpt'),
							'type'        => 'text',
							'required' => array('counter_checkbox_1','equals','1'),
						),
	                   
	                   array(
							'id'          => 'counter_count_1',
							'title'       => __('Count 1:', 'wpt'),
	                        'desc'        => __('Specify the count number for first counter section.', 'wpt'),
							'type'        => 'text',
							'required' => array('counter_checkbox_1','equals','1'),
						),
						
						array(
							'id'          => 'counter_image_1',
							'title'       => __('Image 1:', 'wpt'),
	                        'desc'        => __('Upload image for first counter section.', 'wpt'),
							'type'        => 'media',
							'required' => array('counter_checkbox_1','equals','1'),
						),
						
						array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Counter Section 2', 'wpt'),
					       'indent' => true 
					   ),
	                   
	                     array(
							'id'          => 'counter_checkbox_2',
							'title'       => __('Show Counter Section 2', 'wpt'),
							'subtitle'    => __('Check this to enable second counter section.', 'wpt'),
							'type'        => 'checkbox',
	                        'default'     => '1',
	                    ),
					
	                    array(
							'id'          => 'counter_title_2',
							'title'       => __('Title 2:', 'wpt'),
	                        'desc'        => __('Specify the title for second counter section.', 'wpt'),
							'type'        => 'text',
							'required' => array('counter_checkbox_2','equals','1'),
						),
	                   
	                   array(
							'id'          => 'counter_count_2',
							'title'       => __('Count 2:', 'wpt'),
	                        'desc'        => __('Specify the count number for second counter section.', 'wpt'),
							'type'        => 'text',
							'required' => array('counter_checkbox_2','equals','1'),
						),
						
						array(
							'id'          => 'counter_image_2',
							'title'       => __('Image 2:', 'wpt'),
	                        'desc'        => __('Upload image for second counter section.', 'wpt'),
							'type'        => 'media',
							'required' => array('counter_checkbox_2','equals','1'),
						),
						
						array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Counter Section 3', 'wpt'),
					       'indent' => true 
					   ),
	                   
	                     array(
							'id'          => 'counter_checkbox_3',
							'title'       => __('Show Counter Section 3', 'wpt'),
							'subtitle'    => __('Check this to enable third counter section.', 'wpt'),
							'type'        => 'checkbox',
	                        'default'     => '1',
	                    ),
					
	                    array(
							'id'          => 'counter_title_3',
							'title'       => __('Title 3:', 'wpt'),
	                        'desc'        => __('Specify the title for third counter section.', 'wpt'),
							'type'        => 'text',
							'required' => array('counter_checkbox_3','equals','1'),
						),
	                   
	                   array(
							'id'          => 'counter_count_3',
							'title'       => __('Count 3:', 'wpt'),
	                        'desc'        => __('Specify the count number for third counter section.', 'wpt'),
							'type'        => 'text',
							'required' => array('counter_checkbox_3','equals','1'),
						),
						
						array(
							'id'          => 'counter_image_3',
							'title'       => __('Image 3:', 'wpt'),
	                        'desc'        => __('Upload image for third counter section.', 'wpt'),
							'type'        => 'media',
							'required' => array('counter_checkbox_3','equals','1'),
						),
						
						array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Counter Section 4', 'wpt'),
					       'indent' => true 
					   ),
	                   
	                     array(
							'id'          => 'counter_checkbox_4',
							'title'       => __('Show Counter Section 4', 'wpt'),
							'subtitle'    => __('Check this to enable fourth counter section.', 'wpt'),
							'type'        => 'checkbox',
	                        'default'     => '1',
	                    ),
					
	                    array(
							'id'          => 'counter_title_4',
							'title'       => __('Title 4:', 'wpt'),
	                        'desc'        => __('Specify the title for fourth counter section.', 'wpt'),
							'type'        => 'text',
							'required' => array('counter_checkbox_4','equals','1'),
						),
	                   
	                   array(
							'id'          => 'counter_count_4',
							'title'       => __('Count 4:', 'wpt'),
	                        'desc'        => __('Specify the count number for fourth counter section.', 'wpt'),
							'type'        => 'text',
							'required' => array('counter_checkbox_4','equals','1'),
						),
						
						array(
							'id'          => 'counter_image_4',
							'title'       => __('Image 4:', 'wpt'),
	                        'desc'        => __('Upload image for fourth counter section.', 'wpt'),
							'type'        => 'media',
							'required' => array('counter_checkbox_4','equals','1'),
						),
	                    
	                 ) 
				);
				
	     
				$this->sections[] = array(
					'title' => __('Styling', 'wpt'),
					'icon' => 'el-icon-brush',
					'fields' => array(
						array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Colour Section', 'wpt'),
					       'indent' => true 
					   ),
						array(
							'id'          => 'bodybg_color',
							'title'       => __('Body Background Color', 'wpt'),
							'default' => '#f1501a',
							'type' => 'color',
							'validate' => 'color'
						),
	                    array(
							'id'          => 'headerbg_color',
							'title'       => __('Header Background Color', 'wpt'),
							'default' => '#f1501a',
							'type' => 'color',
							'validate' => 'color'
						),
	                    array(
							'id'          => 'footerbg_color',
							'title'       => __('Footer Background Color', 'wpt'),
							'default' => '#f1501a',
							'type' => 'color',
							'validate' => 'color'
						),
						array(
							'id'          => 'menu_color',
							'title'       => __('Menu Color', 'wpt'),
							'default' => '#f1501a',
							'type' => 'color',
							'validate' => 'color'
						),
						array(
							'id'          => 'active_menu_color',
							'title'       => __('Active Menu Color', 'wpt'),
							'default' => '#f1501a',
							'type' => 'color',
							'validate' => 'color'
						),
						array(
							'id'          => 'breadcrumb_bg_color',
							'title'       => __('Breadcrumb Background Color', 'wpt'),
							'default' => '#f1501a',
							'type' => 'color',
							'validate' => 'color'
						),
						array(
							'id'          => 'link_color',
							'title'       => __('Link  Color', 'wpt'),
							'default' => '#dede2e',
							'type' => 'color',
							'output'    => array(
								        
								        'color'            => 'a'
								    )
							
						),
						array(
							'id'          => 'link_hover_color',
							'title'       => __('Link Hover Color', 'wpt'),
							'default' => '#f1501a',
							'type' => 'color',
							'validate' => 'color',
							'output'    => array(
								        
								        'color'            => 'a'
								    )
						),
						
						array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Typography section', 'wpt'),
					       'indent' => true 
					   ),
	                    array(
	                        'id'          => 'heading_typography',
	                        'type'        => 'typography', 
	                        'title'       => __('Heading Font', 'wpt'),
	                        'google'      => true, 
	                        'font-backup' => true,
	                        'line-height' => false,
	                        'text-align'  => false,
	                        'output'      => array('.about-section .about-overview .overview h3'),
	                        'units'       =>'px',
	                        'subtitle'    => __('Typography option with each property can be called individually.', 'wpt'),
	                        'default'     => array(
	                        'color'       => '#333', 
	                        'font-style'  => '700', 
	                        'font-family' => 'Abel', 
	                        'google'      => true,
	                        'font-size'   => '33px', 
	                        'line-height' => '40'
	                        ),
	                	),
	                     
	                    array(
	                        'id'          => 'content_typography',
	                        'type'        => 'typography', 
	                        'title'       => __('Content Font', 'wpt'),
	                        'google'      => true, 
	                        'font-backup' => true,
	                        'line-height' => false,
	                        'text-align'  => false,
	                        'output'      => array('h2.site-description'),
	                        'units'       =>'px',
	                        'subtitle'    => __('Typography option with each property can be called individually.', 'wpt'),
	                        'default'     => array(
	                        'color'       => '#333', 
	                        'font-style'  => '700', 
	                        'font-family' => 'Abel', 
	                        'google'      => true,
	                        'font-size'   => '33px', 
	                        'line-height' => '40'
	                        ),
	                	),
	                     
	                    array(
	                        'id'          => 'menu_typography',
	                        'type'        => 'typography', 
	                        'title'       => __('Menu Font', 'wpt'),
	                        'google'      => true, 
	                        'font-backup' => true,
	                        'line-height' => false,
	                        'text-align'  => false,
	                        'output'      => array('h2.site-description'),
	                        'units'       =>'px',
	                        'subtitle'    => __('Typography option with each property can be called individually.', 'wpt'),
	                        'default'     => array(
	                        'color'       => '#333', 
	                        'font-style'  => '700', 
	                        'font-family' => 'Abel', 
	                        'google'      => true,
	                        'font-size'   => '33px', 
	                        'line-height' => '40'
	                        ),
	                 ),
					)
				);

				$this->sections[] = array(
					'title' => __('Social & Contact Information', 'wpt'),
					'icon' => 'el-icon-credit-card',
					'fields' => array(
						
						array(
							'id'          => 'phone',
							'title'       => __('Phone Info', 'wpt'),
							'desc'        => __('Type your phone number here.', 'wpt'),
							'type'        => 'text',
						),
						array(
							'id'          => 'email',
							'title'       => __('Email Info', 'wpt'),
							'desc'        => __('Type your email address here.', 'wpt'),
							'type'        => 'text',
						),
						array(
							'id'          => 'linkedin_url',
							'title'       => __('Linkedin Profile', 'wpt'),
	                        'desc'        => __('Type your linkdin link begin with http:// here.', 'wpt'),
							'type'        => 'text',
						),
						array(
							'id'          => 'facebook_url',
							'title'       => __('Facebook Profile', 'wpt'),
	                        'desc'        => __('Type your facebook link begin with http:// here.', 'wpt'),
							'type'        => 'text',
						),
	                    array(
							'id'          => 'twitter_url',
							'title'       => __('Twitter Profile', 'wpt'),
	                        'desc'        => __('Type your twitter link begin with http:// here.', 'wpt'),
							'type'        => 'text',
						),
	                    array(
							'id'          => 'googleplus_url',
							'title'       => __('Google+ Profile', 'wpt'),
	                        'desc'        => __('Type your google+ link begin with http:// here.', 'wpt'),
							'type'        => 'text',
						),
	                    array(
							'id'          => 'youtube_url',
							'title'       => __('Youtube Profile', 'wpt'),
	                        'desc'        => __('Type your youtube link begin with http:// here.', 'wpt'),
							'type'        => 'text',
						),
						array(
							'id'          => 'pinterest_url',
							'title'       => __('Pinterest Profile', 'wpt'),
	                        'desc'        => __('Type your pinterest link begin with http:// here.', 'wpt'),
							'type'        => 'text',
						),
	                    array(
							'id'          => 'blogger_url',
							'title'       => __('Blogger Profile', 'wpt'),
	                        'desc'        => __('Type your blogger link begin with http:// here.', 'wpt'),
							'type'        => 'text',
						),
						
						
					)
				);
				
				$this->sections[] = array(
					'title' => __('Contact Us Information', 'wpt'),
					'icon' => 'el-icon-phone',
					'fields' => array(
						
						/* Location Information */
						array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Location Information', 'wpt'),
					       'indent' => true 
					   ),
					   
						array(
							'id'          => 'location_checkbox',
							'title'       => __('Show Location Section', 'wpt'),
							'desc'        => __('Check this to provide your address.', 'wpt'),
							'type'      => 'checkbox',
							'default'   => '1',
						),
						
						array(
							'id'          => 'location_title',
							'title'       => __('Title', 'wpt'),
							'type'        => 'text',
							'desc'        => __('Provide title for this section.', 'wpt'),
	                        'required'    => array('location_checkbox','equals','1'),
						),   
						
	                    array(
							'id'          => 'location_line_1',
							'title'       => __('Line 1', 'wpt'),
							'type'        => 'text',
	                        'required'    => array('location_checkbox','equals','1'),
						),   
	                   
	                    array(
							'id'          => 'location_line_2',
							'title'       => __('Line 2', 'wpt'),
							'type'        => 'text',
	                        'required'    => array('location_checkbox','equals','1'),
						),   
						
						array(
							'id'          => 'location_line_3',
							'title'       => __('Line 3', 'wpt'),
							'type'        => 'text',
	                        'required'    => array('location_checkbox','equals','1'),
						),   
	                 
	                 /* Contact information */
	                 	array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Contact Information', 'wpt'),
					       'indent' => true 
					   ),
	                 
						array(
							'id'          => 'contact_checkbox',
							'title'       => __('Show Contact Information', 'wpt'),
							'desc'        => __('Check this to provide your contact information.', 'wpt'),
							'type'      => 'checkbox',
							'default'   => '1',
						),
						
						array(
							'id'          => 'contact_title',
							'title'       => __('Title', 'wpt'),
							'type'        => 'text',
							'desc'        => __('Provide title for this section.', 'wpt'),
	                        'required'    => array('location_checkbox','equals','1'),
						),   
						
						array(
							'id'          => 'email_id',
							'title'       => __('Email:', 'wpt'),
							'desc'        => __('Type your email address here.', 'wpt'),
							'type'        => 'text',
							'required'    => array('contact_checkbox','equals','1'),
						),
						array(
							'id'          => 'primary_ph',
							'title'       => __('Primary Phone no.', 'wpt'),
	                        'desc'        => __('Type your primary contact number here.', 'wpt'),
							'type'        => 'text',
							'required'    => array('contact_checkbox','equals','1'),
						),
						array(
							'id'          => 'alternate_ph',
							'title'       => __('Alternate Phone No.', 'wpt'),
	                        'desc'        => __('Type your alternate contact number here.', 'wpt'),
							'type'        => 'text',
							'required'    => array('contact_checkbox','equals','1'),
						),
						
						array(
							'id'          => 'fax_no',
							'title'       => __('Fax No.', 'wpt'),
	                        'desc'        => __('Type your fax number here.', 'wpt'),
							'type'        => 'text',
							'required'    => array('contact_checkbox','equals','1'),
						),
						
						/* office timmings */
						array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Office Hours', 'wpt'),
					       'indent' => true 
					   ),
						array(
							'id'          => 'ofc_hrs_checkbox',
							'title'       => __('Show Office Hours', 'wpt'),
							'desc'        => __('Check this to provide office hour information.', 'wpt'),
							'type'      => 'checkbox',
							'default'   => '1',
						),
						
						array(
							'id'          => 'ofc_hrs_title',
							'title'       => __('Title', 'wpt'),
							'type'        => 'text',
							'desc'        => __('Provide title for this section.', 'wpt'),
	                        'required'    => array('location_checkbox','equals','1'),
						),   
						
						array(
							'id'          => 'weekday',
							'title'       => __('Weekdays:', 'wpt'),
							'desc'        => __('Type weekdays office hours here.', 'wpt'),
							'type'        => 'text',
							'required'    => array('ofc_hrs_checkbox','equals','1'),
						),
						array(
							'id'          => 'weekend',
							'title'       => __('Weekend', 'wpt'),
	                        'desc'        => __('Type your weekend office hours here (if working or else type closed).', 'wpt'),
							'type'        => 'text',
							'required'    => array('ofc_hrs_checkbox','equals','1'),
						),
						
						array(
							'id'          => 'holiday',
							'title'       => __('Public Holidays', 'wpt'),
	                        'desc'        => __('Type your office hours here (if working or else type closed).', 'wpt'),
							'type'        => 'text',
							'required'    => array('ofc_hrs_checkbox','equals','1'),
						),
						
						array(
					       'id' => 'section-start',
					       'type' => 'section',
					       'title' => __('Map', 'wpt'),
					       'indent' => true 
					   ),
						
						/* map information */
						
	                    array(
							'id'          => 'latitude',
							'title'       => __('Latitude', 'wpt'),
	                        'desc'        => __('Type latitude information of your loaction here.', 'wpt'),
							'type'        => 'text',
						),
	                    array(
							'id'          => 'longitude',
							'title'       => __('Longitude', 'wpt'),
	                        'desc'        => __('Type longitude information of your loaction here.', 'wpt'),
							'type'        => 'text',
						),
						
						array(
	    					'id'          => 'marker_image',
	    					'title'       => __('Marker', 'wpt'),
	                        'url'         => false,
	                        'width'       => 150,
	                        'height'      => 150,
	                        'thumbnail'    => true,
	    					'desc'        => __('Change the marker image.', 'wpt'),
	    					'type'        => 'media',
	    					),
						
						array(
							'id'          => 'marker_detail',
							'title'       => __('Marker Detail', 'wpt'),
	                        'desc'        => __('Type details to be displayed on marker here.', 'wpt'),
							'type'        => 'editor',
						),
	                   
					)
				);
	            
	            $this->sections[] = array(
					'title' => __('Backup Settings', 'wpt'),
					'icon' => 'el-icon-refresh',
	                        'fields'    => array(
	                array(
	                    'id'            => 'import_export',
	                    'type'          => 'import_export',
	                    'title'         => 'Import Export',
	                    'subtitle'      => 'Save and restore your Redux options',
	                    'full_width'    => false,
	                    ),
	                )
	                
	            );

		}	


	}
	new Portfolio_Redux_Framework_config();
}

