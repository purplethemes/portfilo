<?php

/*
* Gallery Custom Post type
*/
if ( ! class_exists( 'Slider_Post_Type' ) ) :

	class Slider_Post_Type {

		function __construct() {

			// Runs when the plugin is activated
			register_activation_hook( __FILE__, array( &$this, 'plugin_activation' ) );

			// Add support for translations
			load_plugin_textdomain( 'wpt', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

			// Adds the gallery post type and taxonomies
			add_action( 'init', array( &$this, 'slider_init' ) );

			// Thumbnail support for gallery posts
			add_theme_support( 'post-thumbnails', array( 'gallery' ) );

			 // Adds meta boxes
            add_action( 'add_meta_boxes', array( &$this, 'slider_init_add_metaboxes' ) );
            
            add_filter( 'manage_edit-slide_columns', array( &$this, 'add_slider_thumbnail_column'), 10, 1 );
			add_action( 'manage_posts_custom_column', array( &$this, 'display_slider_thumbnail' ), 10, 1 );
            
            //Save meta-box values
            add_action('save_post', array( &$this, 'save_slider_values' ));
         
         // Give the slider menu item a unique icon
			add_action( 'admin_head', array( &$this, 'slider_icon' ) );
		}

		/**
		 * Flushes rewrite rules on plugin activation to ensure gallery posts don't 404
		 * http://codex.wordpress.org/Function_Reference/flush_rewrite_rules
		 */

		function plugin_activation() {
			$this->slider_init();
			flush_rewrite_rules();
		}

		function slider_init() {

			/**
			 * Enable the Gallery custom post type
			 * http://codex.wordpress.org/Function_Reference/register_post_type
			 */

			$labels = array(
        		'name' => __('Slider Option','wpt'),
        		'singular_name' => __('Slide','wpt'),
        		'add_new' => __('Add New','wpt'),
        		'add_new_item' => __('Add New Slide','wpt'),
        		'edit_item' => __('Edit Slide','wpt'),
        		'new_item' => __('New Slide','wpt'),
        		'view_item' => __('View Slide','wpt'),
        		'search_items' => __('Search Slides','wpt'),
        		'not_found' =>  __('No slides found','wpt'),
        		'not_found_in_trash' => __('No slides found in Trash','wpt'), 
        		'parent_item_colon' => ''
        	  );	
        	  
        	  $args = array(
        		'labels' => $labels,
        		'public' => true,
        		'publicly_queryable' => true,
        		'show_ui' => true, 
        		'query_var' => true,
        		'capability_type' => 'post',
        		'hierarchical' => false,
        		'menu_position' => 5,
        		'menu_icon' => 'dashicons-images-alt',
        		'rewrite' => array( 'slug' => $slider_item_slug, 'with_front'=>true ),
        		'supports' => array( 'title', 'thumbnail', 'page-attributes' ),
        	  ); 
        	  
        	  register_post_type( 'slide', $args );
        }


		/**
		 * Add Columns to Gallery Edit Screen
		 * http://wptheming.com/2010/07/column-edit-pages/
		 */

            function slider_init_add_metaboxes(){
                      
                    add_meta_box("add_slider_meta", "Slider Item Option", array( &$this, 'add_slider_metaboxes' ), "slide", "normal", "low");
                   
        }
        
            function add_slider_metaboxes(){
                      global $post;
                      $custom = get_post_custom($post->ID);
                      $slider_content_title = $custom["slider_content_title"][0];
                      $slider_content_text = $custom["slider_content_text"][0];
                      $slider_button_text = $custom["slider_button_text"][0];
                      $slider_button_link = $custom["slider_button_link"][0];
                   
                      ?>
                      <label><?php _e('Slider Title:', 'wpt');?></label>
                      <input name="slider_content_title" value="<?php echo esc_attr($slider_content_title);?>" />
                      <br/>
                      <em><?php _e('Slide content text.', 'wpt'); ?></em>
                      <br/>
                      <br/>
                      
                      <label><?php _e('Slider Text:', 'wpt');?></label>
                      <input name="slider_content_text" value="<?php echo esc_attr($slider_content_text);?>" />
                      <br/>
                      <em><?php _e('Slide Content Text.', 'wpt'); ?></em>
                      <br/>
                      <br/>
                      
                      <label><?php _e('Button Text:', 'wpt');?></label>
                      <input name="slider_button_text" value="<?php echo esc_attr($slider_button_text);?>" />
                      <br/>
                      <em><?php _e('Slide Button Title.', 'wpt'); ?></em>
                      <br/>
                      <br/>
                      
                      <label><?php _e('Button Link:', 'wpt');?></label>
                      <input name="slider_button_link" value="<?php echo esc_url($slider_button_link);?>" />
                      <br/>
                      <em><?php _e('Slide Button Link.', 'wpt'); ?></em>
                      <br/>
                      <br/> 
                      <?php 
                    } 

           // Add thumbnail to custom column
           
    		function add_slider_thumbnail_column( $slider_columns ) {

    			$column_slider_thumbnail = array( 'slider_featured_image' => __('Slider Thumbnail','wpt' ) );
    			$slider_columns = array_slice( $slider_columns, 0, 1, true ) + $column_slider_thumbnail + array_slice( $slider_columns, 1, NULL, true );
    			return $slider_columns;
    		}

            
            function display_slider_thumbnail($slider_columns) {
                global $post;
                if ($slider_columns == 'slider_featured_image') {
            
                   echo get_the_post_thumbnail( $post->ID, 'cpt-logo-thumbnail' );
            
                }
            }

		
            function save_slider_values( $post_id ){
                 global $post;
                 update_post_meta($post->ID, "slider_content_title", $_POST["slider_content_title"]);
                 update_post_meta($post->ID, "slider_content_text", $_POST["slider_content_text"]);
                 update_post_meta($post->ID, "slider_button_text", $_POST["slider_button_text"]);
                 update_post_meta($post->ID, "slider_button_link", $_POST["slider_button_link"]);
                 
                        
            } 

    		/**
    		 * Displays the custom post type icon in the dashboard
    		 */

		    function slider_icon() { ?>
            <style type="text/css" media="screen">
            #menu-posts-gallery .wp-menu-image {
                background: url(<?php echo get_template_directory_uri(); ?>/img/gallery-icon.png) no-repeat 6px 6px !important;
            }
            #menu-posts-gallery:hover .wp-menu-image, #menu-posts-gallery.wp-has-current-submenu .wp-menu-image {
                background-position:6px -16px !important;
            }
            #icon-edit.icon32-posts-gallery {background: url(<?php echo get_template_directory_uri(); ?>/img/gallery-32x32.png) no-repeat;}
        </style>
		<?php }

	}

	new Slider_Post_Type;

endif;

?>