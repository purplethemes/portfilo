<?php

/*
* Testimonial Custom Post type
*/
if ( ! class_exists( 'Testimonial_Post_Type' ) ) :

	class Testimonial_Post_Type {

		function __construct() {

			// Runs when the plugin is activated
			register_activation_hook( __FILE__, array( &$this, 'plugin_activation' ) );

			// Add support for translations
			load_plugin_textdomain( 'wpt', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

			// Adds the testimonial post type and taxonomies
			add_action( 'init',array( &$this, 'plugin_activation' ) );

			// Thumbnail support for testimonial posts
			add_theme_support( 'post-thumbnails', array( 'testimonial' ) );

			// Adds thumbnails to column view
			add_filter( 'manage_edit-testimonial_columns', array( &$this, 'add_testimonial_thumbnail_column'), 10, 1 );
			add_action( 'manage_posts_custom_column', array( &$this, 'display_testimonial_thumbnail' ), 10, 1 );

			
			// Allows filtering of posts by taxonomy in the admin view
			add_action( 'restrict_manage_posts', array( &$this, 'add_taxonomy_filters' ) );
			
			// Show testimonial post counts in the dashboard
			add_action( 'right_now_content_table_end', array( &$this, 'add_testimonial_counts' ) );

			// Give the testimonial menu item a unique icon
			add_action( 'admin_head', array( &$this, 'testimonial_icon' ) );
		}

		/**
		 * Flushes rewrite rules on plugin activation to ensure testimonial posts don't 404
		 * http://codex.wordpress.org/Function_Reference/flush_rewrite_rules
		 */

		function plugin_activation() {
			$this->testimonial_init();
			flush_rewrite_rules();
		}

		function testimonial_init() {

			/**
			 * Enable the Testimonial custom post type
			 * http://codex.wordpress.org/Function_Reference/register_post_type
			 */

			$labels = array(
				'name' => __( 'Testimonial', 'wpt' ),
				'singular_name' => __( 'Testimonial Item', 'wpt' ),
				'add_new' => __( 'Add New Item', 'wpt' ),
				'add_new_item' => __( 'Add New Testimonial Item', 'wpt' ),
				'edit_item' => __( 'Edit Testimonial Item', 'wpt' ),
				'new_item' => __( 'Add New Testimonial Item', 'wpt' ),
				'view_item' => __( 'View Item', 'wpt' ),
				'search_items' => __( 'Search Testimonial', 'wpt' ),
				'not_found' => __( 'No testimonial items found', 'wpt' ),
				'not_found_in_trash' => __( 'No testimonial items found in trash', 'wpt' )
			);

			$args = array(
				'labels' => $labels,
				'public' => true,
				'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments' ),
				'capability_type' => 'post',
				'rewrite' => array("slug" => "testimonial"), // Permalinks format
                'menu_position' => 5,
                'menu_icon' => 'dashicons-editor-quote',
				'has_archive' => true
			);

			$args = apply_filters('wpt_args', $args);

			register_post_type( 'testimonial', $args );
            
            flush_rewrite_rules();
            
            /**
			 * Register a taxonomy for Testimonial Categories
			 * http://codex.wordpress.org/Function_Reference/register_taxonomy
			 */

			$taxonomy_testimonial_category_labels = array(
				'name' => __( 'Testimonial Categories', 'wpt' ),
				'singular_name' => __( 'Testimonial Category', 'wpt' ),
				'search_items' => __( 'Search Testimonial Categories', 'wpt' ),
				'popular_items' => __( 'Popular Testimonial Categories', 'wpt' ),
				'all_items' => __( 'All Testimonial Categories', 'wpt' ),
				'parent_item' => __( 'Parent Testimonial Category', 'wpt' ),
				'parent_item_colon' => __( 'Parent Testimonial Category:', 'wpt' ),
				'edit_item' => __( 'Edit Testimonial Category', 'wpt' ),
				'update_item' => __( 'Update Testimonial Category', 'wpt' ),
				'add_new_item' => __( 'Add New Testimonial Category', 'wpt' ),
				'new_item_name' => __( 'New Testimonial Category Name', 'wpt' ),
				'separate_items_with_commas' => __( 'Separate testimonial categories with commas', 'wpt' ),
				'add_or_remove_items' => __( 'Add or remove testimonial categories', 'wpt' ),
				'choose_from_most_used' => __( 'Choose from the most used testimonial categories', 'wpt' ),
				'menu_name' => __( 'Testimonial Categories', 'wpt' ),
			);

			$taxonomy_testimonial_category_args = array(
				'labels' => $taxonomy_testimonial_category_labels,
				'public' => true,
				'show_in_nav_menus' => true,
				'show_ui' => true,
				'show_admin_column' => true,
				'show_tagcloud' => true,
				'hierarchical' => true,
				'rewrite' => array( 'slug' => 'testimonial' ),
				'query_var' => true
			);

			register_taxonomy( 'testimonial_category', array( 'testimonial' ), $taxonomy_testimonial_category_args );
	
		}

		/**
		 * Add Columns to Testimonial Edit Screen
		 * http://wptheming.com/2010/07/column-edit-pages/
		 */

           // Add thumbnail to custom column
           
    		function add_testimonial_thumbnail_column( $testimonial_columns ) {

    			$column_testimonial_thumbnail = array( 'featured_image' => __('Testimonial Thumbnail','wpt' ) );
    			$testimonial_columns = array_slice( $testimonial_columns, 0, 1, true ) + $column_testimonial_thumbnail + array_slice( $testimonial_columns, 1, NULL, true );
    			return $testimonial_columns;
    		}

            
            function display_testimonial_thumbnail($testimonial_columns) {
                global $post;
                if ($testimonial_columns == 'testimonial_featured_image') {
            
                   echo get_the_post_thumbnail( $post->ID, 'cpt-logo-thumbnail' );
            
                }
            }


		/**
		 * Add testimonial count to "Right Now" Dashboard Widget
		 */

		    function add_testimonial_counts() {
			if ( ! post_type_exists( 'testimonial' ) ) {
				return;
			}

			$num_posts = wp_count_posts( 'testimonial' );
			$num = number_format_i18n( $num_posts->publish );
			$text = _n( 'Testimonial Item', 'Testimonial Items', intval($num_posts->publish) );
			if ( current_user_can( 'edit_posts' ) ) {
				$num = "<a href='edit.php?post_type=testimonial'>$num</a>";
				$text = "<a href='edit.php?post_type=testimonial'>$text</a>";
			}
			echo '<td class="first b b-testimonial">' . $num . '</td>';
			echo '<td class="t testimonial">' . $text . '</td>';
			echo '</tr>';

			if ($num_posts->pending > 0) {
				$num = number_format_i18n( $num_posts->pending );
				$text = _n( 'Testimonial Item Pending', 'Testimonial Items Pending', intval($num_posts->pending) );
				if ( current_user_can( 'edit_posts' ) ) {
					$num = "<a href='edit.php?post_status=pending&post_type=testimonial'>$num</a>";
					$text = "<a href='edit.php?post_status=pending&post_type=testimonial'>$text</a>";
				}
				echo '<td class="first b b-testimonial">' . $num . '</td>';
				echo '<td class="t testimonial">' . $text . '</td>';

				echo '</tr>';
			}
		}

			/**
		 * Adds taxonomy filters to the testimonial admin page
		 * 
		 */

		function add_taxonomy_filters() {
			global $typenow;
            
			// An array of all the taxonomies you want to display. Use the taxonomy name or slug
			$taxonomies = array( 'testimonial_category');

			// must set this to the post type you want the filter(s) displayed on
			if ( $typenow == 'testimonial' ) {

				foreach ( $taxonomies as $tax_slug ) {
					$current_tax_slug = isset( $_GET[$tax_slug] ) ? $_GET[$tax_slug] : false;
					$tax_obj = get_taxonomy( $tax_slug );
                    
					$tax_name = $tax_obj->labels->name;
                    
					$terms = get_terms($tax_slug);
					if ( count( $terms ) > 0) {
						echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
						echo "<option value=''>$tax_name</option>";
						foreach ( $terms as $term ) {
                           
							echo '<option value=' . $term->slug, $current_tax_slug == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
						}
						echo "</select>";
					}
				}
			}
		}

    		/**
    		 * Displays the custom post type icon in the dashboard
    		 */

		    function testimonial_icon() { ?>
            <style type="text/css" media="screen">
            #menu-posts-testimonial .wp-menu-image {
                background: url(<?php echo get_template_directory_uri(); ?>/img/testimonial-icon.png) no-repeat 6px 6px !important;
            }
            #menu-posts-testimonial:hover .wp-menu-image, #menu-posts-testimonial.wp-has-current-submenu .wp-menu-image {
                background-position:6px -16px !important;
            }
            #icon-edit.icon32-posts-testimonial {background: url(<?php echo get_template_directory_uri(); ?>/img/testimonial-32x32.png) no-repeat;}
        </style>
		<?php }

	}

	new Testimonial_Post_Type;

endif;

?>