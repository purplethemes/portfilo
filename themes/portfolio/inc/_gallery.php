<?php

/*
* Gallery Custom Post type
*/
if ( ! class_exists( 'Gallery_Post_Type' ) ) :

	class Gallery_Post_Type {

		function __construct() {

			// Runs when the plugin is activated
			register_activation_hook( __FILE__, array( &$this, 'plugin_activation' ) );

			// Add support for translations
			load_plugin_textdomain( 'wpt', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

			// Adds the gallery post type and taxonomies
			add_action( 'init',array( &$this, 'plugin_activation' ) );

			// Thumbnail support for gallery posts
			add_theme_support( 'post-thumbnails', array( 'gallery' ) );

			// Adds thumbnails to column view
			add_filter( 'manage_edit-gallery_columns', array( &$this, 'add_gallery_thumbnail_column'), 10, 1 );
			add_action( 'manage_posts_custom_column', array( &$this, 'display_gallery_thumbnail' ), 10, 1 );

			// Show gallery post counts in the dashboard
			add_action( 'right_now_content_table_end', array( &$this, 'add_gallery_counts' ) );

			// Give the gallery menu item a unique icon
			add_action( 'admin_head', array( &$this, 'gallery_icon' ) );
		}

		/**
		 * Flushes rewrite rules on plugin activation to ensure gallery posts don't 404
		 * http://codex.wordpress.org/Function_Reference/flush_rewrite_rules
		 */

		function plugin_activation() {
			$this->gallery_init();
			flush_rewrite_rules();
		}

		function gallery_init() {

			/**
			 * Enable the Gallery custom post type
			 * http://codex.wordpress.org/Function_Reference/register_post_type
			 */

			$labels = array(
				'name' => __( 'Gallery', 'wpt' ),
				'singular_name' => __( 'Gallery Item', 'wpt' ),
				'add_new' => __( 'Add New Item', 'wpt' ),
				'add_new_item' => __( 'Add New Gallery Item', 'wpt' ),
				'edit_item' => __( 'Edit Gallery Item', 'wpt' ),
				'new_item' => __( 'Add New Gallery Item', 'wpt' ),
				'view_item' => __( 'View Item', 'wpt' ),
				'search_items' => __( 'Search Gallery', 'wpt' ),
				'not_found' => __( 'No gallery items found', 'wpt' ),
				'not_found_in_trash' => __( 'No gallery items found in trash', 'wpt' )
			);

			$args = array(
				'labels' => $labels,
				'public' => true,
				'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments','page-attributes' ),
				'capability_type' => 'post',
				'rewrite' => array("slug" => "galleries"), // Permalinks format
                'menu_position' => 5,
                'menu_icon' => 'dashicons-format-gallery',
				'has_archive' => true
			);

			$args = apply_filters('wpt_args', $args);

			register_post_type( 'gallery', $args );
            
            flush_rewrite_rules();

			

		}

		/**
		 * Add Columns to Gallery Edit Screen
		 * http://wptheming.com/2010/07/column-edit-pages/
		 */

           // Add thumbnail to custom column
           
    		function add_gallery_thumbnail_column( $gallery_columns ) {

    			$column_gallery_thumbnail = array( 'featured_image' => __('Gallery Thumbnail','wpt' ) );
    			$gallery_columns = array_slice( $gallery_columns, 0, 1, true ) + $column_gallery_thumbnail + array_slice( $gallery_columns, 1, NULL, true );
    			return $gallery_columns;
    		}

            
            function display_gallery_thumbnail($gallery_columns) {
                global $post;
                if ($gallery_columns == 'featured_image') {
            
                   echo get_the_post_thumbnail( $post->ID, 'cpt-logo-thumbnail' );
            
                }
            }

		/**
		 * Adds taxonomy filters to the gallery admin page
		 * Code artfully lifed from http://pippinsplugins.com
		 */

		  /*  function add_gallery_taxonomy_filters() {
			    global $typenow;

			// An array of all the taxonomyies you want to display. Use the taxonomy name or slug
			$taxonomies = array( 'gallery_category');

			// must set this to the post type you want the filter(s) displayed on
			if ( $typenow == 'gallery' ) {

				foreach ( $taxonomies as $tax_slug ) {
					$current_tax_slug = isset( $_GET[$tax_slug] ) ? $_GET[$tax_slug] : false;
					$tax_obj = get_taxonomy( $tax_slug );
					$tax_name = $tax_obj->labels->name;
					$terms = get_terms($tax_slug);
					if ( count( $terms ) > 0) {
						echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
						echo "<option value=''>$tax_name</option>";
						foreach ( $terms as $term ) {
							echo '<option value=' . $term->slug, $current_tax_slug == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
						}
						    echo "</select>";
					}
				}
			}
		}*/

		/**
		 * Add Gallery count to "Right Now" Dashboard Widget
		 */

		    function add_gallery_counts() {
			if ( ! post_type_exists( 'gallery' ) ) {
				return;
			}

			$num_posts = wp_count_posts( 'gallery' );
			$num = number_format_i18n( $num_posts->publish );
			$text = _n( 'Gallery Item', 'Gallery Items', intval($num_posts->publish) );
			if ( current_user_can( 'edit_posts' ) ) {
				$num = "<a href='edit.php?post_type=gallery'>$num</a>";
				$text = "<a href='edit.php?post_type=gallery'>$text</a>";
			}
			echo '<td class="first b b-gallery">' . $num . '</td>';
			echo '<td class="t gallery">' . $text . '</td>';
			echo '</tr>';

			if ($num_posts->pending > 0) {
				$num = number_format_i18n( $num_posts->pending );
				$text = _n( 'Gallery Item Pending', 'Gallery Items Pending', intval($num_posts->pending) );
				if ( current_user_can( 'edit_posts' ) ) {
					$num = "<a href='edit.php?post_status=pending&post_type=gallery'>$num</a>";
					$text = "<a href='edit.php?post_status=pending&post_type=gallery'>$text</a>";
				}
				echo '<td class="first b b-gallery">' . $num . '</td>';
				echo '<td class="t gallery">' . $text . '</td>';

				echo '</tr>';
			}
		}

    		/**
    		 * Displays the custom post type icon in the dashboard
    		 */

		    function gallery_icon() { ?>
            <style type="text/css" media="screen">
            #menu-posts-gallery .wp-menu-image {
                background: url(<?php echo get_template_directory_uri(); ?>/img/gallery-icon.png) no-repeat 6px 6px !important;
            }
            #menu-posts-gallery:hover .wp-menu-image, #menu-posts-gallery.wp-has-current-submenu .wp-menu-image {
                background-position:6px -16px !important;
            }
            #icon-edit.icon32-posts-gallery {background: url(<?php echo get_template_directory_uri(); ?>/img/gallery-32x32.png) no-repeat;}
        </style>
		<?php }

	}

	new Gallery_Post_Type;

endif;

?>