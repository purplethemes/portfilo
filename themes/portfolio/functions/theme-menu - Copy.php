<?php
/**
 * Menu functions.
 * @package Portfolio
 * @author Purplethemes
 * 
 */

/* ---------------------------------------------------------------------------
 * Registers a menu location to use with navigation menus.
 * --------------------------------------------------------------------------- */
register_nav_menu( 'primary', __( 'Main menu' , 'wpt' ) );
register_nav_menu( 'footer', __( 'Footer menu' , 'wpt' ) );

/* ---------------------------------------------------------------------------
 * Main menu
 * --------------------------------------------------------------------------- */
function wpt_wp_nav_menu() 
{
	$args = array( 
	    'container' 	  => 'nav',
		'container_id'	  => 'menu',
		'menu_class'      => 'nav navbar-nav navbar-right',
		'fallback_cb'	  => 'wpt_wp_page_menu', 
		'theme_location'  => 'primary',
        'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
		'depth' 		  => 0,
        
	);
	wp_nav_menu( $args ); 
}

function wpt_wp_page_menu() 
{
	$args = array(
		'title_li' => '0',
		'sort_column' => 'menu_order',
		'depth' => 0
	);

	echo '<nav id="menu">'."\n";
		echo '<ul>'."\n";
			wp_list_pages($args); 
		echo '</ul>'."\n";
	echo '</nav>'."\n";
}

/* ---------------------------------------------------------------------------
 * Footer menu
* --------------------------------------------------------------------------- */
function wpt_wp_footer_menu()
{
    $menu = wp_get_nav_menu_object( "footer" ); 
	$footer_menu = wp_get_nav_menu_items($menu->term_id);
				
	$flag = 0;
	foreach($footer_menu as $menu)
    {
		if($menu->menu_item_parent == 0)
		{
            if($flag == 1)
            {
                echo '</ul></div>';
            }
                            
            echo '<div class="sitemap">';
			echo '<h5>'.$menu->title.'</h5>';	
			echo '<ul>';
            $flag = 1;
        }
                       
        else
		{                
			echo '<li><a href="'.$menu->url.'">'.$menu->title.'</a></li>';
		}
 	}            
}    

?>