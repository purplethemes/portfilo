<?php

global $portfolio_options;

/* ---------------------------------------------------------------------------
 * Shortcode [portfolio]
 * --------------------------------------------------------------------------- */
function sc_portfolio($attr, $content = null)
{
   extract(shortcode_atts(array(
		'count' => '4',
		'orderby' => 'menu_order',
		'order' => 'DESC',
	), $attr));

	$html = '';
	$args = array( 
    
		'post_type' => 'portfolio',
		'posts_per_page' => intval($count),
		'paged' => -1,
        'post_status'=> 'publish',
		'orderby' => $orderby,
		'order' => $order,	
	);

    $query = new WP_Query($args);
    if ($query->have_posts())
	{ 
        $html .='<article class="project-list">';
        $html .='<section class="row">';
        
        while ($query->have_posts()) 
		{
                    $query->the_post();
                    $term_count = '';
                    $portfolio_terms_deatils = get_the_terms( get_the_ID(), 'portfolio_category');
                    
                    $term_count = count($portfolio_terms_deatils);
                   	$pf_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'portfolio-homepage');
                    $large_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'portfolio-homepagelarge');
                    $pf_imgURL = $pf_imgArray[0];
                    $large_imgURL = $large_imgArray[0];
                    $full_title =  get_the_title();
                    //$no_img = . LIBS_URI .'/theme-mail.php">'
                    $html .='<article class="col-xs-6 col-sm-3 col-md-3">';
                    $html .='<div class="project-container wow fadeInLeft" data-wow-delay="0.4s">';
                    $html .='<div class="pro-img">';
                    
                    if(!empty($pf_imgURL)) $html .='<img src="' .$pf_imgURL. '" class="img-responsive" alt="Project Name" title="Project Name" />';
					else $html .='<img src="' .THEME_URI. '/images/no-img-portfolio.jpg'. '" style="width:253px;height:253px;" class="img-responsive" alt="Project Name" title="Project Name" />';
					
                     if(!empty($large_imgURL))  $html .='<div class="portfolio-entry-hover"> <a class="gallery" href="' .$large_imgURL. '" data-title="'.$full_title.'" data-lightbox="image-3"><i class="glyphicon glyphicon-zoom-in"></i></a>';
                   
                    else $html .='<div class="portfolio-entry-hover"> <a class="gallery" href="' .THEME_URI. '/images/no-img-portfolio.jpg'. '" data-title="'.$full_title.'" data-lightbox="image-3"><i class="glyphicon glyphicon-zoom-in"></i></a>';
                   
                    $html .='</div>';
                    $html .='</div>';
                    $html .='<h4><a href="'. get_permalink() .'">' .the_title(false, false, false). '</a></h4>';
                    
                    if( $portfolio_terms_deatils ){
						$html .='<span>'; 
						foreach( $portfolio_terms_deatils as $category ){
							 $html .='<a href="'. get_term_link($category) .'">' .$category->name. '</a>';
							 if($term_count > 1 && $term_count != '1'){
							 	  $html .= ',';
							 	  $term_count--;
							 } 	  
						}
						$html .='</span>'; 
					}
                    $html .='</div>';
                    $html .='</article>';
				} 
		$html .='</section>';
        $html .='</article>';
          
	}
    
	wp_reset_query();
		
    return $html;
	
}

add_shortcode( 'portfolio', 'sc_portfolio' );



/* ---------------------------------------------------------------------------
 * Shortcode [gallery]
 * --------------------------------------------------------------------------- */
function sc_gallery($attr, $content = null)
{
   extract(shortcode_atts(array(
		'count' => '4',
		'orderby' => 'menu_order',
		'order' => 'DESC',
	), $attr));

	$html = '';
	$args = array( 
    
		'post_type' => 'gallery',
		'posts_per_page' => intval($count),
		'paged' => -1,
        'post_status'=> 'publish',
		'orderby' => $orderby,
		'order' => $order,	
	);

    $query = new WP_Query($args);
    if ($query->have_posts())
	{ 
        $html .='<article class="project-list">';
        $html .='<section class="row">';
        
        while ($query->have_posts()) 
		{
                    $query->the_post();
                    $term_count = '';
                    
                   	$gallery_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'gallery-homepage');
                    $large_imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'gallery-homepagelarge');
                    $gallery_imgURL = $gallery_imgArray[0];
                    $large_imgURL = $large_imgArray[0];
                    $full_title =  get_the_title();
                    $html .='<article class="col-xs-6 col-sm-3 col-md-3">';
                    $html .='<div class="project-container wow fadeInLeft" data-wow-delay="0.4s">';
                    $html .='<div class="pro-img">';
                    
                    if(!empty($gallery_imgURL)) $html .='<img src="'  .$gallery_imgURL. '" class="img-responsive" alt="Project Name" title="Project Name" />';
                    else $html .='<img src="' .THEME_URI. '/images/no-img-portfolio.jpg'. '" style="width:253px;height:253px;" class="img-responsive" alt="Project Name" title="Project Name" />';
                   
                    
                    if(!empty($large_imgURL)) $html .='<div class="portfolio-entry-hover"> <a class="gallery" href="' .$large_imgURL. '" data-title="'. $full_title. '" data-lightbox="portfolio-homepage"><i class="glyphicon glyphicon-zoom-in"></i></a>';
                    else $html .='<div class="portfolio-entry-hover"> <a class="gallery" href="' .THEME_URI. '/images/no-img-portfolio.jpg'. '" data-title="'. $full_title. '" data-lightbox="portfolio-homepage"><i class="glyphicon glyphicon-zoom-in"></i></a>';
                    
                    $html .='</div>';
                    $html .='</div>';
                    $html .='<h4>' .the_title(false, false, false). '</h4>';
                   
                    $html .='</div>';
                    $html .='</article>';
				} 
		$html .='</section>';
        $html .='</article>';
          
	}
    
	wp_reset_query();
		
    return $html;
	
}

add_shortcode( 'gallery', 'sc_gallery' );



/* ---------------------------------------------------------------------------
 * Shortcode [testimonial]
 * --------------------------------------------------------------------------- */
function sc_testimonial($attr, $content = null) 
{
	global $portfolio_options;
	
	extract(shortcode_atts(array(
		'number' => '4',
		'order' => 'date',
		'category' => '0',
    ), $atts));
	
	
	$args=array(
		'post_type' => 'testimonial',
		'showposts' => $number,
		'orderby' => $order,
	);
	
	if(!empty($category)) {
		$args['tax_query'][]=array(
            'taxonomy' => 'testimonial_category',
            'terms' => $category,
            'field' => 'term_id',
        );
	}
	
	$query = new WP_Query($args);
    if ($query->have_posts())
	{ 
	
		
	    	$html .='<section class="exp-section">';
		        $html .='<div class="title-area wow  fadeIn">';
		            $html .='<h2 class="section-title">' .$portfolio_options['testimonial_section_title'].'</h2>';
		            $html .='<div class="section-divider divider-inside-top"></div>';
		            $html .= $portfolio_options['testimonial_section_content']? '<p class="section-sub-text">'.$portfolio_options['testimonial_section_content'].'</p>' : '';
		            
		        	$html .='</div>';
					
					$html .='<section class="testimonials-slider wow flipInX" data-wow-delay="0.3s">';
					
					while($query->have_posts()){
						$query->the_post();
					
						ob_start();
						get_template_part('libs/content', 'testimonial');
						$html .=ob_get_contents();
						ob_end_clean();
				}
			
	}
    
	wp_reset_query();
	
	
	$html .='</section>';	
    $html .='</section>';
    
	return $html;
}

add_shortcode('testimonial', 'sc_testimonial');



/* ---------------------------------------------------------------------------
 * Shortcode [counter]
 * --------------------------------------------------------------------------- */
function sc_counter($attr, $content = null) 
{
	
	$html  = '<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>';
	
	global $portfolio_options;

	$cnt = 0;
	for($i=1; $i<=4; $i++){
		if(!empty($portfolio_options['counter_checkbox_'.$i]))
		{
			$cnt++;	
		}
	}

	switch($cnt){
	    case '1':
			$class = 'col-xs-12 col-sm-6 col-md-12';
	        break;
		case '2':
			$class = 'col-xs-12 col-sm-6 col-md-6';
			break;
		case '3':
			$class = 'col-xs-12 col-sm-6 col-md-4';
			break;
		case '4':
			$class = 'col-xs-12 col-sm-6 col-md-3';
			break;
	    default :
	        $class = '';
			break;
	}

	if($cnt != 0) {
		$html .='<section class="counter-section">';
    		$html .='<div class="container">';
        		$html .='<section class="row" id="counter-area">';
           
		             for($i=1; $i<=4; $i++){ 
		            
			            if($portfolio_options['counter_checkbox_'.$i]) {
			            
							$html .='<article class="'  .$class. '">';
			                    $html .='<div class="counter-block wow fadeInUp" data-wow-delay="0.4s">';
			                         if($portfolio_options['counter_image_'.$i]['url'] && $portfolio_options['counter_image_'.$i]['url'] ){ 
			                         	$html .='<img src="' .$portfolio_options['counter_image_'.$i]['url']. '" />';
			               			 } 
			               			 else { 
			               			 	echo ''; 
			               			 } 
			                        if($portfolio_options['counter_count_'.$i] && $portfolio_options['counter_checkbox_'.$i] ){ 
			                        	$html .='<p class="cnt-no">' .$portfolio_options['counter_count_'.$i]. '</p>';
			                        	 
			                         } 
			                         else { 
			                         	echo '';
			                         } 
			                        
			                        if($portfolio_options['counter_title_'.$i] && $portfolio_options['counter_checkbox_'.$i] ){ 
				                        $html .='<p class="cnt-title">'.$portfolio_options['counter_title_'.$i]. '</p>';
				                     
			                        } 
			                        else { 
			                        	echo ''; 
			                        	} 
			                    $html .='</div>';
			                $html .='</article>';
						 }
		            	
					} 
			
				$html .='</section>';
   		 	$html .='</div>';
		$html .='</section>';

		return $html;

	}
	
} 
add_shortcode('counter', 'sc_counter');
?>