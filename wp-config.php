<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

define('DB_NAME', 'wpportfoliotheme_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$iRtU;?MgD(F,q p`*=(x7!s*Ny~B$^l1+WD~@}JuDLxA_}KrpM#D=zib5mba]|O');
define('SECURE_AUTH_KEY',  ' R(4L^xB[I;U$9-34N!*M.hd|-66KwYC81fW<EXx/Wk?Y|c}JfuBmu}~-M+T>Uy1');
define('LOGGED_IN_KEY',    'U!$Ga(ilU-CO%Ff<f4AcC/qb>>4:foUv?,FQFe&.2 +w+-2c&d&CehAgyw[GI!c|');
define('NONCE_KEY',        '(DO+_#s8K@=T{7*N/%t#!tx.KWT,/nX.bq*]]jUmQu?t:0CtIsMM_4wf7wX0M{F-');
define('AUTH_SALT',        's7L}7f}~W,<+~2dazOkgl6*iZ GgDxxY%9!5a$)U&H-x-J5IfU%PF-TNlycincz,');
define('SECURE_AUTH_SALT', 'DIXkuUQ$cg^m=hg6zK6qK5e5l;INMSVykj!b8/HKES0]UERG47lA2]Ux_x:EJs=7');
define('LOGGED_IN_SALT',   '~<;&/PMM?Crz)E=#5h#@k|%)w0h=O-u7pAS`DPZCxk_/}&AB]d*Mz0-EF p%4$Xc');
define('NONCE_SALT',       '~|}^_|56rA@l$ts!3(qkJT-Z-!}Y1,%ZDS;pmnRep,ficO/EFber5{:Dvo~8QY?i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
